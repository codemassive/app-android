package app.planomy.cmassive.de.planomy.common;

/**
 * Created by johan on 19.04.2018.
 */

public interface ClickHandler<T> {

    void onClick(T object);

}
