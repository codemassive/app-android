package app.planomy.cmassive.de.planomy.model.feature;

/**
 * Created by johan on 08.04.2018.
 */

public class VpxEndpoint {

    private String url;

    public VpxEndpoint() {

    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
