package app.planomy.cmassive.de.planomy.ui.licence;

import android.support.v7.widget.RecyclerView;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import app.planomy.cmassive.de.planomy.R;
import app.planomy.cmassive.de.planomy.ui.recycler.SimpleDataProvider;
import app.planomy.cmassive.de.planomy.ui.recycler.SimpleItem;

public class LicenceAdapter extends RecyclerView.Adapter<LicenceAdapter.LicenceViewHolder> {

    private List<LicenceItem> items;

    public LicenceAdapter(List<LicenceItem> items) {
        this.items = items;
    }

    @Override
    public LicenceAdapter.LicenceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.licence_item, parent, false);
        return new LicenceAdapter.LicenceViewHolder(view);
    }

    @Override
    public void onBindViewHolder(LicenceAdapter.LicenceViewHolder holder, int position) {
        LicenceItem item = items.get(position);
        holder.visName(View.VISIBLE, item.getName());
        if(item.getCopyright() != null) {
            holder.visCopyright(View.VISIBLE, item.getCopyright());
        } else {
            holder.visCopyright(View.GONE, null);
        }
        if(item.getLicence() != null) {
            holder.visLicence(View.VISIBLE, item.getLicence());
        } else {
            holder.visLicence(View.GONE, null);
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class LicenceViewHolder extends RecyclerView.ViewHolder {

        public ImageView nameImage;
        public TextView nameText;

        public ImageView copyrightImage;
        public TextView copyrightText;

        public ImageView licenceImage;
        public TextView licenceText;


        public LicenceViewHolder(View itemView) {
            super(itemView);

            nameImage = itemView.findViewById(R.id.licence_image_name);
            nameText = itemView.findViewById(R.id.licence_text_name);

            copyrightImage = itemView.findViewById(R.id.licence_image_copyright);
            copyrightText = itemView.findViewById(R.id.licence_text_copyright);

            licenceImage = itemView.findViewById(R.id.licence_image_licence);
            licenceText = itemView.findViewById(R.id.licence_text_licence);
        }

        public void visName(int vis, CharSequence text) {
            nameImage.setVisibility(vis);
            nameText.setText(text);
            nameText.setVisibility(vis);
        }

        public void visCopyright(int vis, CharSequence text) {
            copyrightImage.setVisibility(vis);
            copyrightText.setText(text);
            copyrightText.setVisibility(vis);
        }

        public void visLicence(int vis, CharSequence text) {
            licenceText.setClickable(true);
            licenceText.setMovementMethod(LinkMovementMethod.getInstance());

            licenceImage.setVisibility(vis);
            licenceText.setText(text);
            licenceText.setVisibility(vis);
        }
    }
}
