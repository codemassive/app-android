package app.planomy.cmassive.de.planomy.manager.local;

import app.planomy.cmassive.de.planomy.model.settings.VpxSettings;

public class LocalSubscriptionSettings {

    private String name;
    private int id;
    private VpxSettings settings;

    public LocalSubscriptionSettings() {

    }

    public LocalSubscriptionSettings(String name, int id, VpxSettings settings) {
        this.name = name;
        this.id = id;
        this.settings = settings;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public VpxSettings getSettings() {
        return settings;
    }

    public void setSettings(VpxSettings settings) {
        this.settings = settings;
    }
}
