package app.planomy.cmassive.de.planomy.ui.added;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import app.planomy.cmassive.de.planomy.R;
import app.planomy.cmassive.de.planomy.common.ClickHandler;
import app.planomy.cmassive.de.planomy.manager.SchoolManager;
import app.planomy.cmassive.de.planomy.model.feature.VpxFeature;
import app.planomy.cmassive.de.planomy.model.school.VpxSchool;
import app.planomy.cmassive.de.planomy.ui.news.NewsActivity;
import app.planomy.cmassive.de.planomy.ui.substitution.SubstitutionActivity;

public class FeatureSelectActivity extends AppCompatActivity implements ClickHandler<VpxFeature> {

    private VpxSchool school;

    private RecyclerView recyclerView;
    private TextView warningText;

    public static final String EXTRA_SCHOOL_ID = "EXTRA_SCHOOL_ID";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feature_select);
        setTitle("Meine Features");

        this.warningText = findViewById(R.id.feature_no);
        this.recyclerView = findViewById(R.id.feature_list);
        this.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        this.recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        handleIntent(getIntent());
    }

    @Override
    public void onClick(VpxFeature object) {
        Intent intent = null;

        if(object.getName().contains("ft-news")) {
            intent = new Intent(this, NewsActivity.class);
        } else if(object.getName().contains("ft-substitution")) {
            intent = new Intent(this, SubstitutionActivity.class);
        }

        if(intent != null) {
            intent.putExtra(EXTRA_SCHOOL_ID, this.school.getSchoolId());
            startActivity(intent);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);

        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        if(intent != null && intent.hasExtra(AddedSchoolsBrowseFragment.EXTRA_SCHOOL_ID)) {
            school = SchoolManager.getSchool(intent.getIntExtra(AddedSchoolsBrowseFragment.EXTRA_SCHOOL_ID, -1));
        }

        if(school != null) {
            if (school.getSubscribedFeatures().size() > 0) {
                this.warningText.setVisibility(View.GONE);
            } else {
                this.warningText.setVisibility(View.VISIBLE);
            }

            this.recyclerView.setAdapter(new FeatureRecyclerAdapter(school, this));
        } else {
            finish();
        }
    }
}
