package app.planomy.cmassive.de.planomy.ui;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import app.planomy.cmassive.de.planomy.R;
import app.planomy.cmassive.de.planomy.common.ClickHandler;
import app.planomy.cmassive.de.planomy.common.TaskObserver;
import app.planomy.cmassive.de.planomy.manager.SchoolManager;
import app.planomy.cmassive.de.planomy.model.school.VpxSchool;
import app.planomy.cmassive.de.planomy.tasks.ImageViewUrlTask;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class SchoolItemRecyclerViewAdapter extends RecyclerView.Adapter<SchoolItemRecyclerViewAdapter.ViewHolder> {

    private final ClickHandler<VpxSchool> mListener;
    private List<VpxSchool> schools = new ArrayList<>();

    public SchoolItemRecyclerViewAdapter(ClickHandler<VpxSchool> listener) {
        mListener = listener;
        schools = SchoolManager.getSchools();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.school_fragment_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final VpxSchool school = this.schools.get(position);
        if(school == null) return;

        holder.mItem = school;
        holder.mNameView.setText(school.getSchoolInfo().getName() + " (" + school.getSchoolInfo().getShortName() + ")");
        holder.mLocationView.setText(school.getSchoolAddress().getCity() + ", " + new Locale("",  school.getSchoolAddress().getCountry()).getDisplayCountry(Locale.getDefault()));

        if(school.getCachedBitmap() != null) {
            holder.profileImage.setImageBitmap(school.getCachedBitmap());
        } else {
            try {
                new ImageViewUrlTask(new URL(school.getSchoolInfo().getPictureUrl()), holder.profileImage, new TaskObserver<Bitmap>() {
                    @Override
                    public void onSuccess(Bitmap bitmap) {
                        school.setCachedBitmap(bitmap);
                    }

                    @Override
                    public void onError(Exception ex) {

                    }
                }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onClick(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return schools.size();
    }

    public void setQuery(String query, boolean notify) {
        this.schools = SchoolManager.searchSchool(query);
        if(notify) notifyDataSetChanged();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        public final View mView;
        public final TextView mNameView;
        public final TextView mLocationView;
        public VpxSchool mItem;
        public ImageView profileImage;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mNameView = (TextView) view.findViewById(R.id.school_name);
            mLocationView = (TextView) view.findViewById(R.id.school_location);
            profileImage = (ImageView) view.findViewById(R.id.school_image);
        }
    }
}
