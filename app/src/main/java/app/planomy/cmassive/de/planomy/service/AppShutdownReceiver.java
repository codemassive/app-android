package app.planomy.cmassive.de.planomy.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import app.planomy.cmassive.de.planomy.manager.UserDataManager;

public class AppShutdownReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        UserDataManager.saveData();
    }
}
