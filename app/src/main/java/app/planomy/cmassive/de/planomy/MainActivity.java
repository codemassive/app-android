package app.planomy.cmassive.de.planomy;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import app.planomy.cmassive.de.planomy.manager.UserDataManager;
import app.planomy.cmassive.de.planomy.service.RestJob;
import app.planomy.cmassive.de.planomy.ui.SchoolBrowseFragment;
import app.planomy.cmassive.de.planomy.ui.about.AboutActivity;
import app.planomy.cmassive.de.planomy.ui.developer.DeveloperInfoActivity;
import app.planomy.cmassive.de.planomy.ui.added.AddedSchoolsBrowseFragment;
import io.hypertrack.smart_scheduler.Job;
import io.hypertrack.smart_scheduler.SmartScheduler;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, TabLayout.OnTabSelectedListener {

    private SchoolBrowseFragment schoolBrowseFragment = new SchoolBrowseFragment();
    private AddedSchoolsBrowseFragment addedSchoolsBrowseFragment = new AddedSchoolsBrowseFragment();

    private ProgressBar progressBar;
    private NavigationView navigationView;
    private TabLayout tabLayout;
    private FloatingActionButton floatingActionButton;

    private static ActivityDisplayState state = ActivityDisplayState.MY_SCHOOLS;

    private static Context staticContext;
    private SmartScheduler smartScheduler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        staticContext = getApplicationContext();

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        this.floatingActionButton = findViewById(R.id.main_action_button);

        tabLayout = findViewById(R.id.main_tabs);
        tabLayout.addOnTabSelectedListener(this);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        int resId = this.state.getResourceId();
        if(getIntent() != null) {
            if(getIntent().hasExtra("state")) {
                String navigate = getIntent().getStringExtra("state");
                resId = ActivityDisplayState.valueOf(navigate).getResourceId();
            }
        }

        if(state.getTabPos() != -1) {
            TabLayout.Tab tab = tabLayout.getTabAt(state.getTabPos());
            tab.select();
            onTabSelected(tab);
        } else {
            onNavigationItemSelected(navigationView.getMenu().findItem(resId));
        }

        this.progressBar = findViewById(R.id.progressBar);
        this.smartScheduler = SmartScheduler.getInstance(this);

        if(!smartScheduler.contains(RestJob.JOB_ID)) {
            Job.Builder builder = new Job.Builder(RestJob.JOB_ID, new RestJob(), Job.Type.JOB_TYPE_HANDLER, "app.planomy.cmassive.de.planomy.task")
                    .setIntervalMillis(RestJob.JOB_INTERVAL)
                    .setRequiresCharging(false)
                    .setRequiredNetworkType(Job.NetworkType.NETWORK_TYPE_CONNECTED)
                    .setPeriodic(RestJob.JOB_INTERVAL, 0);

            Job job = builder.build();
            if(!smartScheduler.addJob(job)) {
                Toast.makeText(getApplicationContext(), "App nicht voll funktionsfähig: Interner Fehler!", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        UserDataManager.saveData();
    }

    @Override
    protected void onStop() {
        super.onStop();
        UserDataManager.saveData();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        int id = item.getItemId();

        if(id == R.id.nav_schools_browse) {
            selectSchoolBrowse();
            drawer.closeDrawer(GravityCompat.START);
        } else if(id == R.id.nav_schools) {
            selectMySchools();
            drawer.closeDrawer(GravityCompat.START);
        } else if(id == R.id.nav_information) {
            Intent intent = new Intent(this, AboutActivity.class);
            startActivity(intent);
        } else if(id == R.id.nav_developer) {
            Intent intent = new Intent(this, DeveloperInfoActivity.class);
            startActivity(intent);
        }

        if(state.getTabPos() != -1) {
            TabLayout.Tab tab = tabLayout.getTabAt(state.getTabPos());
            tab.select();
        }

        return true;
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        if(tab.getPosition() == 0) {
            selectMySchools();
        } else if(tab.getPosition() == 1) {
            selectSchoolBrowse();
        }
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    private void selectSchoolBrowse() {
        loadFragmentInMainContainer(getSupportFragmentManager(), R.id.main_fragment_container2, schoolBrowseFragment, false);
        this.state = ActivityDisplayState.SCHOOL_BROWSE;
        setTitle("Schulen suchen");

        this.floatingActionButton.setImageResource(R.drawable.ic_send_black_24dp);
        this.floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new SubmitDialog(MainActivity.this).show();
            }
        });
    }

    private void selectMySchools() {
        loadFragmentInMainContainer(getSupportFragmentManager(), R.id.main_fragment_container2, addedSchoolsBrowseFragment, false);
        this.state = ActivityDisplayState.MY_SCHOOLS;

        setTitle("Meine Schulen");

        this.floatingActionButton.setImageResource(R.drawable.ic_add_black_24dp);
        this.floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onNavigationItemSelected(navigationView.getMenu().findItem(R.id.nav_schools_browse));
            }
        });
    }

    public static void loadFragmentInMainContainer(FragmentManager manager, @IdRes int idRes, Fragment fragment, boolean backStack) {
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(idRes, fragment);
        if(backStack) transaction.addToBackStack(null);
        transaction.commit();
    }

    public static Context getStaticContext() {
        return staticContext;
    }

    public static void setStaticContext(Context staticContext) {
        MainActivity.staticContext = staticContext;
    }

    private enum ActivityDisplayState {

        MY_SCHOOLS(0, R.id.nav_schools),
        SCHOOL_BROWSE(1, R.id.nav_schools_browse);

        private int tabPos;
        private int resId;

        private ActivityDisplayState(int tabPos, @IdRes int resId) {
            this.resId = resId;
            this.tabPos = tabPos;
        }

        public int getResourceId() {
            return resId;
        }
        public int getTabPos() { return tabPos; }
    }
}
