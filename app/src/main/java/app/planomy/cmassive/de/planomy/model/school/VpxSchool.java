package app.planomy.cmassive.de.planomy.model.school;


import android.graphics.Bitmap;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import app.planomy.cmassive.de.planomy.manager.SubscriptionManager;
import app.planomy.cmassive.de.planomy.manager.UserDataManager;
import app.planomy.cmassive.de.planomy.model.feature.VpxFeature;

public class VpxSchool {

    //GENERAL INFO
    private int schoolId;
    private List<VpxFeature> features;
    private ArrayList<String> schoolClasses = new ArrayList<>();

    //BASIC INFO
    @JsonProperty("info")
    private VpxInfo schoolInfo;

    @JsonProperty("address")
    private VpxAddress schoolAddress;

    @JsonProperty("contact")
    private VpxContact schoolContact;

    @JsonIgnore
    private Bitmap cachedBitmap;

    public VpxSchool() {

    }

    public List<VpxFeature> getFeatures() {
        return features;
    }

    public void setFeatures(List<VpxFeature> features) {
        this.features = features;
    }

    public int getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(int schoolId) {
        this.schoolId = schoolId;
    }

    public VpxInfo getSchoolInfo() {
        return schoolInfo;
    }

    public void setSchoolInfo(VpxInfo schoolInfo) {
        this.schoolInfo = schoolInfo;
    }

    public VpxAddress getSchoolAddress() {
        return schoolAddress;
    }

    public void setSchoolAddress(VpxAddress schoolAddress) {
        this.schoolAddress = schoolAddress;
    }

    public VpxContact getSchoolContact() {
        return schoolContact;
    }

    public void setSchoolContact(VpxContact schoolContact) {
        this.schoolContact = schoolContact;
    }

    public Bitmap getCachedBitmap() {
        return cachedBitmap;
    }

    public void setCachedBitmap(Bitmap cachedBitmap) {
        this.cachedBitmap = cachedBitmap;
    }

    public ArrayList<String> getSchoolClasses() {
        return schoolClasses;
    }

    public void setSchoolClasses(ArrayList<String> schoolClasses) {
        this.schoolClasses = schoolClasses;
    }

    public List<VpxFeature> getSubscribedFeatures() {
        ArrayList<VpxFeature> features = new ArrayList<>();
        for(VpxFeature feature : this.features) {
            if(SubscriptionManager.getSubscription(schoolId, feature.getName()) != null) {
                features.add(feature);
            }
        }

        return features;
    }
}
