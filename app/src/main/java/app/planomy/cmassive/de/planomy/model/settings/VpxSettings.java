package app.planomy.cmassive.de.planomy.model.settings;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.util.HashMap;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY)
@JsonSubTypes({
        @JsonSubTypes.Type(value = VpxSubstitutionSettings.class, name = "VPX_SUBSTITUTION_SETTINGS"),
        @JsonSubTypes.Type(value = VpxDefaultSettings.class, name = "VPX_DEFAULT_SETTINGS"),
        @JsonSubTypes.Type(value = VpxNewsSettings.class, name = "VPX_NEWS_SETTINGS")
})
public abstract class VpxSettings {


}
