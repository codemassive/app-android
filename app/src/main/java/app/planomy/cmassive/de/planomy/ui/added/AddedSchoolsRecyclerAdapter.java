package app.planomy.cmassive.de.planomy.ui.added;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.AdapterDataObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import app.planomy.cmassive.de.planomy.R;
import app.planomy.cmassive.de.planomy.common.ClickHandler;
import app.planomy.cmassive.de.planomy.manager.SchoolManager;
import app.planomy.cmassive.de.planomy.manager.SubscriptionManager;
import app.planomy.cmassive.de.planomy.model.school.VpxSchool;

public class AddedSchoolsRecyclerAdapter extends RecyclerView.Adapter<AddedSchoolsRecyclerAdapter.AddedSchoolsViewHolder> {

    private ArrayList<VpxSchool> schools = new ArrayList<>();
    private ClickHandler<VpxSchool> clickHandler;

    public AddedSchoolsRecyclerAdapter(ClickHandler<VpxSchool> clickHandler) {
        super();
        this.clickHandler = clickHandler;

        update();
        registerAdapterDataObserver(new AdapterDataObserver() {
            @Override
            public void onChanged() {
                update();
            }
        });
    }

    private void update() {
        schools = (ArrayList<VpxSchool>) SchoolManager.getSchoolsWithSubscriptions();
    }

    @Override
    public AddedSchoolsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.added_schools_item, parent, false);
        return new AddedSchoolsViewHolder(view);    }

    @Override
    public void onBindViewHolder(AddedSchoolsViewHolder holder, int position) {
        final VpxSchool school = schools.get(position);

        holder.name.setText(school.getSchoolInfo().getName());
        if(school.getCachedBitmap() != null) {
            holder.mainImage.setImageBitmap(school.getCachedBitmap());
        }

        if(SubscriptionManager.getSubscription(school.getSchoolId(), "ft-news") == null) {
            holder.newsImage.setColorFilter(ContextCompat.getColor(holder.view.getContext(), R.color.colorNotActive));
        }

        if(SubscriptionManager.getSubscription(school.getSchoolId(), "ft-substitution") == null) {
            holder.subImage.setColorFilter(ContextCompat.getColor(holder.view.getContext(), R.color.colorNotActive));
        }

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickHandler.onClick(school);
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.schools.size();
    }



    public class AddedSchoolsViewHolder extends RecyclerView.ViewHolder {

        public View view;
        public TextView name;
        public ImageView mainImage;
        public ImageView subImage;
        public ImageView newsImage;

        public AddedSchoolsViewHolder(View itemView) {
            super(itemView);

            this.view = itemView;
            name = view.findViewById(R.id.added_name);
            mainImage = view.findViewById(R.id.added_image);
            subImage = view.findViewById(R.id.added_image_substitutions);
            newsImage = view.findViewById(R.id.added_image_news);
        }
    }

}
