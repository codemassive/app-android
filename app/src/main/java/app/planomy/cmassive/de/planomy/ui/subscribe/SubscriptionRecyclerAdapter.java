package app.planomy.cmassive.de.planomy.ui.subscribe;

import app.planomy.cmassive.de.planomy.R;
import app.planomy.cmassive.de.planomy.common.ClickHandler;
import app.planomy.cmassive.de.planomy.common.rest.RestCommon;
import app.planomy.cmassive.de.planomy.manager.SubscriptionManager;
import app.planomy.cmassive.de.planomy.manager.UserDataManager;
import app.planomy.cmassive.de.planomy.model.feature.VpxFeature;
import app.planomy.cmassive.de.planomy.model.school.VpxSchool;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;

public class SubscriptionRecyclerAdapter extends RecyclerView.Adapter<SubscriptionRecyclerAdapter.ViewHolder> {

    private VpxSchool vpxSchool;
    private ClickHandler<ClickObjectBundle> clickHandler;

    public SubscriptionRecyclerAdapter(VpxSchool vpxSchool, ClickHandler<ClickObjectBundle> featureClickHandler) {
        this.vpxSchool = vpxSchool;
        this.clickHandler = featureClickHandler;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.subscription_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final VpxFeature feature = this.vpxSchool.getFeatures().get(position);

        holder.onOffSwitch.setOnCheckedChangeListener(null);
        holder.onOffSwitch.setClickable(true);

        holder.progressBar.setVisibility(View.GONE);


        String humanName = RestCommon.internalNameToHumanName(feature.getName());
        holder.nameText.setText(humanName);

        if(humanName.startsWith("???")) {
            holder.statusImage.setImageResource(R.drawable.ic_warning);
            holder.onOffSwitch.setChecked(false);
        } else {
            if(!feature.isAuthRequired()) {
                holder.statusImage.setImageResource(R.drawable.ic_lock_open);
            } else {
                holder.statusImage.setImageResource(R.drawable.ic_lock);
            }
        }

        if(SubscriptionManager.getSubscription(vpxSchool.getSchoolId(), feature.getName()) != null) {
            holder.onOffSwitch.setChecked(true);
        } else {
            holder.onOffSwitch.setChecked(false);
        }

        if(this.clickHandler != null) {
            holder.onOffSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    clickHandler.onClick(new ClickObjectBundle(holder, feature));
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return vpxSchool.getFeatures().size();
    }

    public VpxSchool getVpxSchool() {
        return vpxSchool;
    }

    public void setVpxSchool(VpxSchool vpxSchool) {
        this.vpxSchool = vpxSchool;
    }

    public class ClickObjectBundle {

        public ViewHolder holder;
        public VpxFeature feature;

        public ClickObjectBundle(ViewHolder holder, VpxFeature feature) {
            this.holder = holder;
            this.feature = feature;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public View view;
        public ImageView statusImage;
        public ProgressBar progressBar;
        public TextView nameText;
        public Switch onOffSwitch;

        public ViewHolder(View itemView) {
            super(itemView);

            this.view = itemView;
            this.statusImage = view.findViewById(R.id.sub_image);
            this.progressBar = view.findViewById(R.id.sub_progress);
            this.nameText = view.findViewById(R.id.sub_text_name);
            this.onOffSwitch = view.findViewById(R.id.sub_switch_onoff);
        }
    }

}
