package app.planomy.cmassive.de.planomy.ui.settings;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Switch;

import java.util.ArrayList;

import app.planomy.cmassive.de.planomy.R;
import app.planomy.cmassive.de.planomy.manager.SchoolManager;
import app.planomy.cmassive.de.planomy.model.school.VpxSchool;
import app.planomy.cmassive.de.planomy.model.settings.VpxSubstitutionSettings;
import app.planomy.cmassive.de.planomy.model.user.VpxSubscription;


public class SubstitutionSettingsActivity extends SettingsActivityBase {

    private Switch notifySwitch;
    private Spinner spinner;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_substitution_settings);

        notifySwitch = findViewById(R.id.settings_notify);
        spinner = findViewById(R.id.settings_sub_class);
    }

    @Override
    public void saveSettings(VpxSubscription subscription) {
        VpxSubstitutionSettings settings = new VpxSubstitutionSettings();
        if(subscription.getLocalSettings() != null) settings = (VpxSubstitutionSettings) subscription.getLocalSettings();

        settings.setNotifications(notifySwitch.isChecked());
        settings.setSchoolClass(((spinner.getSelectedItem() != null && spinner.getSelectedItemPosition() != 0) ? (String) spinner.getSelectedItem() : null));
        subscription.setLocalSettings(settings);
    }

    @Override
    public void loadSettings(VpxSubscription subscription) {
        VpxSchool school = SchoolManager.getSchool(subscription.getSchoolId());
        ArrayList<String> classes = new ArrayList<>();
        classes.add("Alle");
        classes.addAll(school.getSchoolClasses());
        spinner.setAdapter(new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, classes));

        if(subscription.getLocalSettings() != null) {
            VpxSubstitutionSettings settings = (VpxSubstitutionSettings) subscription.getLocalSettings();

            notifySwitch.setChecked(settings.isNotifications());
            if(settings.getSchoolClass() != null) {
                spinner.setSelection(classes.indexOf(settings.getSchoolClass()));
            }
        }
    }
}
