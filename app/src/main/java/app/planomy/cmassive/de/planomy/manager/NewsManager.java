package app.planomy.cmassive.de.planomy.manager;

import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;

import app.planomy.cmassive.de.planomy.model.news.News;

public class NewsManager {

    private static HashMap<Integer, ArrayList<News>> NEWS = new HashMap<>();

    public static void addNews(int schoolId, News news) {
        ArrayList<News> list = NEWS.get(schoolId);
        if(list == null) {
            list = new ArrayList<>();
        }

        list.add(news);
        NEWS.put(schoolId, list);
    }

    public static void deleteNews(int schoolId, News news) {
        ListIterator<News> newsListIterator = NEWS.get(schoolId).listIterator();
        while(newsListIterator.hasNext()) {
            if(newsListIterator.next().getNewsId() == news.getNewsId()) {
                newsListIterator.remove();
                return;
            }
        }
    }

    public static List<News> getNewsForSchool(int schoolId) {
        if(NEWS.containsKey(schoolId)) {
            return NEWS.get(schoolId);
        }

        return new ArrayList<>();
    }

    public static News getNews(int id) {
        for(ArrayList<News> nl : NEWS.values()) {
            for(News n : nl) {
                if(n.getNewsId() == id) {
                    return n;
                }
            }
        }
        return null;
    }

    public static HashMap<Integer, ArrayList<News>> getNews() {
        return NEWS;
    }
}
