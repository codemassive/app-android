package app.planomy.cmassive.de.planomy.common.rest;

import android.util.Log;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.joda.JodaModule;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import app.planomy.cmassive.de.planomy.manager.UserDataManager;

public class PlanomyRestCall<RESPONSE> {

    public static final String GET = "GET";
    public static final String POST = "POST";
    public static final String DELETE = "DELETE";

    private static final Pattern REPLACE_GROUP_PATTERN = Pattern.compile("(\\{.*\\})");
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    private String method;
    private String postData;
    private URL url;
    private LinkedList<String> params = new LinkedList<>();

    private final Class<RESPONSE> responseClass;

    static {
        OBJECT_MAPPER.registerModule(new JodaModule());
    }

    public PlanomyRestCall(URL url, String method, Class<RESPONSE> responseClass) {
        this.url = url;
        this.method = method;
        this.responseClass = responseClass;
    }

    public PlanomyRestResponse<RESPONSE> call() throws Exception {
        String urlString = url.toString();
        Matcher matcher = REPLACE_GROUP_PATTERN.matcher(urlString);
        int pos = 0;
        while(matcher.find()) {
            urlString.replace(matcher.group(1), params.get(pos));
            pos++;
        }

        HttpURLConnection connection = (HttpURLConnection) new URL(urlString).openConnection();
        prepareConnection(connection);

        if(this.postData != null) {
            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
            writer.write(this.postData);
            writer.close();
        }

        StringBuffer buffer = new StringBuffer();
        BufferedReader inputStream = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String line = null;
        while((line = inputStream.readLine()) != null) {
            buffer.append(line);
        }

        inputStream.close();
        connection.disconnect();

        PlanomyRestResponse<RESPONSE> response = new PlanomyRestResponse<>(connection.getResponseCode(), OBJECT_MAPPER.readValue(buffer.toString(), responseClass));
        return response;
    }

    private void prepareConnection(HttpURLConnection connection) throws ProtocolException {
        connection.setRequestMethod(this.method);
        connection.setInstanceFollowRedirects(true);
        connection.setReadTimeout(2000);
        connection.setConnectTimeout(1000);

        connection.setRequestProperty("Content-Type", "application/json");
        connection.setRequestProperty("User-Agent","Mozilla/5.0 ( compatible ) ");
        connection.setRequestProperty("Accept","*/*");

        if(UserDataManager.hasDataBeenLoaded()) {
            connection.setRequestProperty("VPX_USER_IDENT", UserDataManager.getUserId());
        }
        if(this.method.equalsIgnoreCase(POST)) {
            connection.setDoOutput(true);
        }
    }

    public LinkedList<String> getParams() {
        return params;
    }

    public void setParams(LinkedList<String> params) {
        this.params = params;
    }

    public String getPostData() {
        return postData;
    }

    public void setPostData(String postData) {
        this.postData = postData;
    }

    public URL getUrl() {
        return url;
    }

    public void setUrl(URL url) {
        this.url = url;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }
}
