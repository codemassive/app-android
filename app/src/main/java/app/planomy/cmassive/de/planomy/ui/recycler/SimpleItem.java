package app.planomy.cmassive.de.planomy.ui.recycler;

public class SimpleItem {

    private String header;
    private String content;
    private String tag;

    public SimpleItem(String header, String content, String tag) {
        this.header = header;
        this.content = content;
        this.tag = tag;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }
}
