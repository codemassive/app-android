package app.planomy.cmassive.de.planomy.tasks;

import android.os.AsyncTask;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import app.planomy.cmassive.de.planomy.common.rest.PlanomyUrls;
import app.planomy.cmassive.de.planomy.model.user.VpxSubscription;

public class UserDataLoaderTask extends AsyncTask<Void, Void, UserDataLoaderTask.UserDataBundle> {

    private String userId;

    public UserDataLoaderTask(String userId) {
        this.userId = userId;
    }

    @Override
    protected UserDataBundle doInBackground(Void ...voids) {
        return fetchCommonData(userId);
    }

    public static UserDataBundle fetchCommonData(String userId) {
        UserDataBundle bundle = new UserDataBundle();

        HttpComponentsClientHttpRequestFactory httpRequestFactory = new HttpComponentsClientHttpRequestFactory();
        httpRequestFactory.setConnectTimeout(5000);
        httpRequestFactory.setReadTimeout(5000);
        RestTemplate template = new RestTemplate(httpRequestFactory);
        template.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

        LinkedMultiValueMap<String, String> linkedMultiValueMap = new LinkedMultiValueMap<>();
        linkedMultiValueMap.add("VPX_USER_IDENT", userId);
        HttpEntity<VpxSubscription[]> entity = new HttpEntity<VpxSubscription[]>(linkedMultiValueMap);

        ResponseEntity<VpxSubscription[]> responseEntity = template.exchange(PlanomyUrls.API_USER_SUBSCRIPTIONS.getUrlString(), HttpMethod.GET, entity, VpxSubscription[].class);
        bundle.setSubscriptionList(Arrays.asList(responseEntity.getBody()));
        return bundle;
    }

    public static class UserDataBundle {

        private List<VpxSubscription> subscriptionList = new ArrayList<>();

        public List<VpxSubscription> getSubscriptionList() {
            return subscriptionList;
        }

        public void setSubscriptionList(List<VpxSubscription> subscriptionList) {
            this.subscriptionList = subscriptionList;
        }
    }
}
