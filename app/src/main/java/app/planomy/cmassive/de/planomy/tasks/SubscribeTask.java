package app.planomy.cmassive.de.planomy.tasks;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;

import app.planomy.cmassive.de.planomy.common.rest.PlanomyUrl;
import app.planomy.cmassive.de.planomy.common.rest.PlanomyUrls;
import app.planomy.cmassive.de.planomy.common.rest.RestCommon;
import app.planomy.cmassive.de.planomy.common.TaskObserver;
import app.planomy.cmassive.de.planomy.model.feature.VpxFeature;
import app.planomy.cmassive.de.planomy.model.user.VpxSubscription;

public class SubscribeTask extends AsyncTask<Void, Void, Void> {

    private final TaskObserver<VpxSubscription> observer;
    private int schoolId;
    private VpxFeature feature;
    private HashMap<String, String> loginData;
    private SubscriptionMethod method;
    private VpxSubscription subscription;

    public SubscribeTask(SubscriptionMethod method, @NonNull TaskObserver<VpxSubscription> observer, VpxFeature feature) {
        this.method = method;
        this.observer = observer;
        this.feature = feature;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        RestTemplate template = RestCommon.defaultJacksonTemplate();

       try {
           if(this.method == SubscriptionMethod.SUBSCRIBE) {
               SubscriptionTaskPostData postData = new SubscriptionTaskPostData(loginData, schoolId, feature.getName());
               HttpEntity<VpxSubscription> entity = RestCommon.<VpxSubscription>defaultEntity(postData);
               ResponseEntity<VpxSubscription> responseEntity = template.postForEntity(PlanomyUrls.API_USER_SUBSCRIPTIONS.getUrlString(), entity, VpxSubscription.class);

               observer.onSuccess(responseEntity.getBody());
           } else {
               template.exchange(PlanomyUrls.API_USER_SUBSCRIPTION.getUrlString(), HttpMethod.DELETE, RestCommon.defaultEntity(), Object.class, this.subscription.getSubscriptionId());
               observer.onSuccess(null);
           }
       } catch(Exception ex) {
           ex.printStackTrace();
           observer.onError(ex);
       }

        return null;
    }

    public TaskObserver getObserver() {
        return observer;
    }

    public int getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(int schoolId) {
        this.schoolId = schoolId;
    }

    public VpxFeature getFeature() {
        return feature;
    }

    public void setFeature(VpxFeature feature) {
        this.feature = feature;
    }

    public HashMap<String, String> getLoginData() {
        return loginData;
    }

    public void setLoginData(HashMap<String, String> loginData) {
        this.loginData = loginData;
    }

    public SubscriptionMethod getMethod() {
        return method;
    }

    public void setMethod(SubscriptionMethod method) {
        this.method = method;
    }

    public VpxSubscription getSubscription() {
        return subscription;
    }

    public void setSubscription(VpxSubscription subscription) {
        this.subscription = subscription;
    }

    public static enum SubscriptionMethod {

        SUBSCRIBE,
        UNSUBSCRIBE

    }

    private class SubscriptionTaskPostData {

        private HashMap<String, String> data;
        private int schoolId;
        private String featureName;

        public SubscriptionTaskPostData(HashMap<String, String> loginData, int schoolId, String featureName) {
            this.data = loginData;
            this.schoolId = schoolId;
            this.featureName = featureName;
        }

        public HashMap<String, String> getData() {
            return data;
        }

        public void setData(HashMap<String, String> data) {
            this.data = data;
        }

        public int getSchoolId() {
            return schoolId;
        }

        public void setSchoolId(int schoolId) {
            this.schoolId = schoolId;
        }

        public String getFeatureName() {
            return featureName;
        }

        public void setFeatureName(String featureName) {
            this.featureName = featureName;
        }
    }
}
