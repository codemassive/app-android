package app.planomy.cmassive.de.planomy.ui.settings;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import app.planomy.cmassive.de.planomy.manager.SubscriptionManager;
import app.planomy.cmassive.de.planomy.manager.UserDataManager;
import app.planomy.cmassive.de.planomy.model.user.VpxSubscription;

public abstract class SettingsActivityBase extends AppCompatActivity {

    private VpxSubscription subscription = null;

    @Override
    protected void onPause() {
        super.onPause();

        saveSettingsWrapper(subscription);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        saveSettingsWrapper(subscription);
    }

    @Override
    protected void onResume() {
        super.onResume();

        setTitle("Einstellungen");

        if(getIntent() != null || getIntent().hasExtra("EXTRA_SUB_ID")) {
            this.subscription = SubscriptionManager.getSubscription(getIntent().getIntExtra("EXTRA_SUB_ID", -1));
        }

        if(this.subscription == null) {
            finish();
            return;
        }

        loadSettings(subscription);
    }

    private void saveSettingsWrapper(VpxSubscription subscription) {
        saveSettings(subscription);
        UserDataManager.saveData();
    }

    public abstract void saveSettings(VpxSubscription subscription);
    public abstract void loadSettings(VpxSubscription subscription);
}
