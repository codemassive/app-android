package app.planomy.cmassive.de.planomy.ui.added;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import app.planomy.cmassive.de.planomy.R;
import app.planomy.cmassive.de.planomy.common.ClickHandler;
import app.planomy.cmassive.de.planomy.manager.SchoolManager;
import app.planomy.cmassive.de.planomy.model.school.VpxSchool;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddedSchoolsBrowseFragment extends Fragment implements ClickHandler<VpxSchool> {

    private RecyclerView recyclerView;

    public static final String EXTRA_SCHOOL_ID = "EXTRA_SCHOOL_ID";

    public AddedSchoolsBrowseFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_added_schools_browse, container, false);

        this.recyclerView = view.findViewById(R.id.added_schools_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        recyclerView.setAdapter(new AddedSchoolsRecyclerAdapter(this));

        Log.w("Warn", "ENABLE LIST FRAGMENT");

        if(SchoolManager.getSchoolsWithSubscriptions().size() > 0) {
            view.findViewById(R.id.added_schools_no).setVisibility(View.GONE);
        }

        return view;
    }

    @Override
    public void onClick(VpxSchool object) {
        Intent intent = new Intent(getContext(), FeatureSelectActivity.class);
        intent.putExtra(EXTRA_SCHOOL_ID, object.getSchoolId());
        startActivity(intent);
    }
}
