package app.planomy.cmassive.de.planomy.ui.about;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;

import app.planomy.cmassive.de.planomy.R;
import app.planomy.cmassive.de.planomy.common.ClickHandler;
import app.planomy.cmassive.de.planomy.ui.licence.LicenceActivity;
import app.planomy.cmassive.de.planomy.ui.recycler.SectionAdapter;
import app.planomy.cmassive.de.planomy.ui.recycler.SectionDivider;
import app.planomy.cmassive.de.planomy.ui.recycler.SimpleDataProvider;
import app.planomy.cmassive.de.planomy.ui.recycler.SimpleItem;


public class AboutActivity extends AppCompatActivity implements ClickHandler<SimpleItem> {

    private RecyclerView recyclerView;

    private static LinkedHashMap<String, LinkedList<SimpleItem>> DATA = new LinkedHashMap<>();

    static {
        LinkedList<SimpleItem> generalList = new LinkedList<>();
        generalList.add(new SimpleItem("App Version", "1.0", "version"));
        generalList.add(new SimpleItem("Entwickler", "Johannes Quast", "developer"));

        LinkedList<SimpleItem> developerList = new LinkedList<>();
        developerList.add(new SimpleItem("Lizenzinformationen", null, "licence"));

        DATA.put("Allgemein", generalList);
        DATA.put("Regulatorisches", developerList);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        SimpleDataProvider provider = new SimpleDataProvider(this);
        provider.setData(DATA);
        SectionAdapter adapter = new SectionAdapter(provider);

        this.recyclerView = findViewById(R.id.about_list);
        this.recyclerView.setAdapter(adapter);
        this.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        this.recyclerView.addItemDecoration(new SectionDivider(this, DividerItemDecoration.VERTICAL));
    }

    @Override
    public void onClick(SimpleItem object) {
        if(object.getTag() != null) {
            if(object.getTag().equalsIgnoreCase("licence")) {
                Intent intent = new Intent(this, LicenceActivity.class);
                startActivity(intent);
            }
        }
    }
}
