package app.planomy.cmassive.de.planomy.ui.recycler;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

public interface SectionDataProvider {

    int getSectionCount();
    int getSectionItemCount(int section);

    RecyclerView.ViewHolder createSectionViewHolder(ViewGroup parent);
    RecyclerView.ViewHolder createItemViewHolder(ViewGroup parent);

    void bindSectionViewHolder(RecyclerView.ViewHolder holder, int section);
    void bindItemViewHolder(RecyclerView.ViewHolder holder, int section, int relPos, int absPos);

    void updateRequested();
}
