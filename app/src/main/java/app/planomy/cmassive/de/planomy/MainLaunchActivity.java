package app.planomy.cmassive.de.planomy;

import android.content.Intent;
import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.apache.http.conn.ConnectTimeoutException;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.ExecutionException;

import app.planomy.cmassive.de.planomy.common.TaskObserver;
import app.planomy.cmassive.de.planomy.common.rest.PlanomyUrl;
import app.planomy.cmassive.de.planomy.common.rest.PlanomyUrls;
import app.planomy.cmassive.de.planomy.manager.SchoolManager;
import app.planomy.cmassive.de.planomy.manager.SubscriptionManager;
import app.planomy.cmassive.de.planomy.manager.UserDataManager;
import app.planomy.cmassive.de.planomy.model.school.VpxSchool;
import app.planomy.cmassive.de.planomy.tasks.SchoolLoaderTask;
import app.planomy.cmassive.de.planomy.tasks.UserDataLoaderTask;

public class MainLaunchActivity extends AppCompatActivity {

    private Button retryButton;
    private TextView statusText;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MainActivity.setStaticContext(getApplicationContext());

        setContentView(R.layout.activity_main_launch);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setTitle("Login");

        this.retryButton = findViewById(R.id.launch_retry);
        this.statusText = findViewById(R.id.launch_status);
        this.progressBar = findViewById(R.id.lauch_progress);

        this.statusText.setText("");
        this.retryButton.setVisibility(View.GONE);
        this.retryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                retryButton.setVisibility(View.GONE);
                loadApp();
            }
        });

        loadApp();
    }

    private void setStatus(final String s) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                statusText.setText(s);
            }
        });
    }

    private void loadApp() {
        progressBar.setVisibility(View.VISIBLE);

        SchoolManager.getSchools().clear();
        SubscriptionManager.getSubscriptions().clear();

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    loadSchools();

                    if(startLogin()) {
                        UserDataLoaderTask.UserDataBundle bundle = UserDataLoaderTask.fetchCommonData(UserDataManager.getUserId());
                        if(bundle != null) {
                            SubscriptionManager.addSubscriptions(bundle.getSubscriptionList());
                        } else {
                            loginFailed("Laden der Logindaten fehlgeschlagen!");
                        }

                        setStatus("Geladen!");
                        finishLoading();
                    }
                } catch(Exception ex) {
                    ex.printStackTrace();

                    Exception check = ex;
                    if(ex instanceof ResourceAccessException) {
                        check = (Exception) ((ResourceAccessException) ex).getRootCause();
                    }

                    if(check instanceof ConnectTimeoutException) {
                        loginFailed("Zeitüberschreitung!");
                    } else if(check instanceof IOException) {
                        loginFailed("Fehler beim Übertragen der Daten!");
                    } else if(check instanceof HttpClientErrorException) {
                        loginFailed("Verbindung fehlgeschlagen!");
                    } else {
                        loginFailed("Unbekannter Fehler!");
                    }
                }
            }
        }).start();
    }

    private void finishLoading() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressBar.setVisibility(View.GONE);

                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);

                finish();
            }
        });
    }

    private void loadSchools() throws Exception {
        setStatus("Lade Schulen...");
        VpxSchool[] schools = SchoolLoaderTask.loadSchools();
        for(VpxSchool school : schools) {
            SchoolManager.addSchool(school);
        }
    }

    private boolean startLogin() {
        setStatus("Starte login...");
        UserDataManager.loadData();

        String userId = UserDataManager.getUserId();
        if(userId == null || userId.isEmpty() || !isUserIdApiValid(userId)) {
            setStatus("Registriere...");
            userId = initRegister();
            if(userId == null || userId.isEmpty()) {
                loginFailed("Registrierung fehlgeschlagen!");
                return false;
            }

            UserDataManager.setUserId(userId);
            UserDataManager.saveData();
        }

        return true;
    }

    private void loginFailed(String reason) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                retryButton.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
            }
        });
        setStatus(reason);
    }

    private String initRegister() {
        HttpComponentsClientHttpRequestFactory httpRequestFactory = new HttpComponentsClientHttpRequestFactory();
        httpRequestFactory.setConnectTimeout(5000);
        httpRequestFactory.setReadTimeout(5000);
        RestTemplate template = new RestTemplate(httpRequestFactory);
        template.getMessageConverters().add(new StringHttpMessageConverter());

        try {
            String content = template.postForEntity(PlanomyUrls.API_REGISTER.getUrlString(), null, String.class).getBody();
            if(content == null) return null;
            JSONObject object = new JSONObject(content);
            return object.getString("userId");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    private boolean isUserIdApiValid(String userId) {
        PlanomyUrl connect = PlanomyUrls.API_USER;
        try {
            HttpURLConnection connection = (HttpURLConnection) connect.getUrl().openConnection();
            connection.setRequestProperty("VPX_USER_IDENT", userId);
            connection.setRequestMethod("GET");

            if(connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                Log.w("Warn", "RESPONSE CODE " + Integer.toString(connection.getResponseCode()));
                return false;
            }

            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
}
