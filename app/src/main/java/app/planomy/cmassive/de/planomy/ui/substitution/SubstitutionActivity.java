package app.planomy.cmassive.de.planomy.ui.substitution;

import android.support.design.widget.TabLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.joda.time.LocalDateTime;

import app.planomy.cmassive.de.planomy.R;
import app.planomy.cmassive.de.planomy.common.TaskObserver;
import app.planomy.cmassive.de.planomy.manager.SchoolManager;
import app.planomy.cmassive.de.planomy.manager.SubscriptionManager;
import app.planomy.cmassive.de.planomy.manager.SubstitutionManager;
import app.planomy.cmassive.de.planomy.model.school.VpxSchool;
import app.planomy.cmassive.de.planomy.model.settings.VpxSubstitutionSettings;
import app.planomy.cmassive.de.planomy.model.substitution.VpxSubstitutionDay;
import app.planomy.cmassive.de.planomy.model.user.VpxSubscription;
import app.planomy.cmassive.de.planomy.tasks.SubstitutionLoaderTask;
import app.planomy.cmassive.de.planomy.ui.added.FeatureSelectActivity;
import app.planomy.cmassive.de.planomy.ui.recycler.SectionAdapter;

public class SubstitutionActivity extends AppCompatActivity implements TabLayout.OnTabSelectedListener {

    private TabLayout tableLayout;
    private TextView noChanges;

    private VpxSchool school;
    private VpxSubscription subscription;

    private SectionAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_substitution);
        getSupportActionBar().setElevation(0);

        if(getIntent() != null) {
            school = SchoolManager.getSchool(getIntent().getIntExtra(FeatureSelectActivity.EXTRA_SCHOOL_ID, -1));
        }

        if(school == null) {
            finish();
            return;
        }

        noChanges = findViewById(R.id.sub_no_changes);
        this.subscription = SubscriptionManager.getSubscription(school.getSchoolId(), "ft-substitution");

        adapter = new SectionAdapter(new SubstitutionProvider(this.subscription));
        RecyclerView view = findViewById(R.id.sub_recycler_view);
        view.setLayoutManager(new LinearLayoutManager(this));
        view.setAdapter(adapter);

        SwipeRefreshLayout swipeRefreshLayout = findViewById(R.id.sub_refresh);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new SubstitutionLoaderTask(school.getSchoolId(), new TaskObserver<VpxSubstitutionDay[]>() {
                    @Override
                    public void onSuccess(VpxSubstitutionDay[] vpxSubstitutionDays) {
                        swipeRefreshLayout.setRefreshing(false);
                        adapter.notifyDataSetChanged();

                        Toast.makeText(SubstitutionActivity.this, "Vertretungsplan aktualisiert.", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(Exception ex) {
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }).execute();
            }
        });

        tableLayout = findViewById(R.id.substitution_tabs);
        tableLayout.addOnTabSelectedListener(this);

        int dow = LocalDateTime.now().getDayOfWeek();
        changeDay(dow);
        if(dow <= 5) {
            tableLayout.getTabAt(dow-1).select();
        }

        setTitle("Vertertungsplan (" + school.getSchoolInfo().getShortName() + ")");
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        changeDay(tab.getPosition()+1);
    }

    private void changeDay(int dayOfWeek) {
        ((SubstitutionProvider) adapter.getProvider()).setDayOfWeek(dayOfWeek);
        adapter.notifyDataSetChanged();

        if(((SubstitutionProvider) adapter.getProvider()).getData().size() > 0) {
            noChanges.setVisibility(View.GONE);
        } else {
            noChanges.setVisibility(View.VISIBLE);
        }

        String cls = null;
        if(subscription.getLocalSettings() != null) {
            VpxSubstitutionSettings settings = (VpxSubstitutionSettings) subscription.getLocalSettings();
            cls = settings.getSchoolClass();
        }

        LinearLayout tabStrip = (LinearLayout) tableLayout.getChildAt(0);
        for(int i = 1; i <= 5; i++) {
            View tabView = tabStrip.getChildAt(i-1);
            if(tabView == null) continue;

            if(SubstitutionManager.countSubstitutionsForWeekday(this.school.getSchoolId(), i, cls) == 0) {
                tabView.setVisibility(View.GONE);
            } else {
                tabView.setVisibility(View.VISIBLE);
            }
        }

    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }
}
