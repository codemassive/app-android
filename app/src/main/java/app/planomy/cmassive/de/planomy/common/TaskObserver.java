package app.planomy.cmassive.de.planomy.common;

/**
 * Created by johan on 11.04.2018.
 */

public interface TaskObserver<T> {

    void onSuccess(T t);
    void onError(Exception ex);
}
