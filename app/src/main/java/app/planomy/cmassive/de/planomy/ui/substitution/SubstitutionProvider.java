package app.planomy.cmassive.de.planomy.ui.substitution;

import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;

import app.planomy.cmassive.de.planomy.R;
import app.planomy.cmassive.de.planomy.manager.SubstitutionManager;
import app.planomy.cmassive.de.planomy.model.settings.VpxSubstitutionSettings;
import app.planomy.cmassive.de.planomy.model.substitution.VpxSubstitution;
import app.planomy.cmassive.de.planomy.model.user.VpxSubscription;
import app.planomy.cmassive.de.planomy.ui.recycler.SectionDataProvider;

public class SubstitutionProvider implements SectionDataProvider {

    private int dayOfWeek = 0;
    private int schoolId;
    private VpxSubscription subscription;
    private LinkedHashMap<String, ArrayList<VpxSubstitution>> data = new LinkedHashMap<>();

    public SubstitutionProvider(VpxSubscription subscription) {
        this.schoolId = subscription.getSchoolId();
        this.subscription = subscription;

        updateRequested();
    }

    @Override
    public int getSectionCount() {
        return data.size();
    }

    @Override
    public int getSectionItemCount(int section) {
        int i = 0;
        for(String s : data.keySet()) {
            if(i == section) return data.get(s).size();
            i++;
        }

        return 0;
    }

    @Override
    public RecyclerView.ViewHolder createSectionViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.substitution_item_header, parent, false);
        return new HeaderViewHolder(view);
    }

    @Override
    public RecyclerView.ViewHolder createItemViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.substitution_item, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void bindSectionViewHolder(RecyclerView.ViewHolder holder, int section) {
        HeaderViewHolder textViewHolder = (HeaderViewHolder) holder;
        int i = 0;
        for(String s : data.keySet()) {
            if(i == section) {
                textViewHolder.text.setText(s);
                return;
            }
            i++;
        }
    }

    @Override
    public void bindItemViewHolder(RecyclerView.ViewHolder holder, int section, int relPos, int absPos) {
        ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
        int i = 0;
        for(String s : data.keySet()) {
            if(i == section) {
                itemViewHolder.visRoom(View.GONE, null);
                itemViewHolder.visSubject(View.GONE, null);
                itemViewHolder.visTeacher(View.GONE, null);
                itemViewHolder.visInfo(View.GONE, null);

                ArrayList<VpxSubstitution> substitutions = data.get(s);
                VpxSubstitution substitution = substitutions.get(relPos);

                itemViewHolder.timeText.setText(substitution.getDateTime().toString("HH:mm"));

                boolean isCancelled = substitution.getNewRoom() == null && substitution.getNewSubject() == null && substitution.getNewTeacher().isEmpty();
                if(isCancelled) { //Ausfall
                    itemViewHolder.layout.setBackgroundResource(R.color.colorCancelled);
                    itemViewHolder.visSubject(View.VISIBLE, "AUSFALL");
                } else {
                    if(substitution.getNewTeacher() != null) {
                        itemViewHolder.visTeacher(View.VISIBLE, substitution.getNewTeacher().toArray()[0].toString());
                    }
                    if(substitution.getNewRoom() != null) {
                        itemViewHolder.visRoom(View.VISIBLE, substitution.getNewRoom());
                    }
                    if(substitution.getNewSubject() != null) {
                        itemViewHolder.visSubject(View.VISIBLE, substitution.getNewSubject());
                    }
                    if(substitution.getDescription() != null) {
                        itemViewHolder.visInfo(View.VISIBLE, substitution.getDescription());
                    }

                    /*if(substitution.getNewSubject() != null && (substitution.getOldRoom() == null && substitution.getOldSubject() == null && substitution.getOldTeacher().isEmpty())) {
                        itemViewHolder.layout.setBackgroundResource(R.color.colorAdditional);
                    } else {
                        itemViewHolder.layout.setBackgroundResource(R.color.colorPrimary);
                    }*/

                    itemViewHolder.layout.setBackgroundResource(R.color.colorDefaultSub);
                }
                return;
            }
            i++;
        }
    }

    @Override
    public void updateRequested() {
        VpxSubstitutionSettings settings = (VpxSubstitutionSettings) this.subscription.getLocalSettings();
        String cls = null;
        if(settings != null) {
            cls = settings.getSchoolClass();
        }

        this.data = SubstitutionManager.groupByClassesAndWeekday(this.schoolId, cls, dayOfWeek);
        sortData();
    }

    private void sortData() {
        if(data != null) {
            for(String s : data.keySet()) {
                ArrayList<VpxSubstitution> substitutions = data.get(s);
                Collections.sort(substitutions);
                data.put(s, substitutions);
            }
        }
    }

    public LinkedHashMap<String, ArrayList<VpxSubstitution>> getData() {
        return data;
    }

    public void setDayOfWeek(int dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        public ConstraintLayout layout;

        public TextView timeText;

        public ImageView subjectImage;
        public TextView subjectText;

        public ImageView roomImage;
        public TextView roomText;

        public ImageView teacherImage;
        public TextView teacherText;

        public ImageView infoImage;
        public TextView infoText;

        public ItemViewHolder(View itemView) {
            super(itemView);

            layout = itemView.findViewById(R.id.sub_const_layout);
            timeText = itemView.findViewById(R.id.substitution_time);

            subjectImage = itemView.findViewById(R.id.substitution_subject_image);
            subjectText = itemView.findViewById(R.id.substitution_subject);

            roomImage = itemView.findViewById(R.id.substitution_room_image);
            roomText = itemView.findViewById(R.id.substitution_room);

            teacherImage = itemView.findViewById(R.id.substitution_teacher_image);
            teacherText = itemView.findViewById(R.id.substitution_teacher);

            infoImage = itemView.findViewById(R.id.substitution_info_image);
            infoText = itemView.findViewById(R.id.substitution_info);
        }

        public void visTeacher(int v, String text) {
            teacherImage.setVisibility(v);
            teacherText.setVisibility(v);

            if(v == View.VISIBLE && text != null) {
                teacherText.setText(text);
            }
        }

        public void visSubject(int v, String text) {
            subjectText.setVisibility(v);
            subjectImage.setVisibility(v);

            if(v == View.VISIBLE && text != null) {
                subjectText.setText(text);
            }
        }

        public void visRoom(int v, String text) {
            roomImage.setVisibility(v);
            roomText.setVisibility(v);

            if(v == View.VISIBLE && text != null) {
                roomText.setText(text);
            }
        }

        public void visInfo(int v, String text) {
            infoImage.setVisibility(v);
            infoText.setVisibility(v);

            if(v == View.VISIBLE && text != null) {
                infoText.setText(text);
            }
        }
    }

    public class HeaderViewHolder extends RecyclerView.ViewHolder {

        public TextView text;

        public HeaderViewHolder(View itemView) {
            super(itemView);

            text = itemView.findViewById(R.id.substitution_header_class);
        }
    }

}
