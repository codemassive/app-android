package app.planomy.cmassive.de.planomy.common.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.datatype.joda.JodaModule;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequest;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URI;

import app.planomy.cmassive.de.planomy.manager.UserDataManager;

public class RestCommon {

    private static RestTemplate template = defaultJacksonTemplate();

    public static HttpEntity defaultEntity(Object body) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("VPX_USER_IDENT", UserDataManager.getUserId());
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity entity = new HttpEntity<>(body, headers);
        return entity;
    }

    public static HttpEntity defaultEntity() {
        return defaultEntity(null);
    }

    public static RestTemplate defaultJacksonTemplate(int connectTimeout, int readTimeout) {
        HttpComponentsClientHttpRequestFactory httpRequestFactory =
                new HttpComponentsClientHttpRequestFactory();
        httpRequestFactory.setConnectTimeout(connectTimeout);
        httpRequestFactory.setReadTimeout(readTimeout);

        RestTemplate template = new RestTemplate(httpRequestFactory);
        template.setRequestFactory(httpRequestFactory);

        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        ObjectMapper mapper = converter.getObjectMapper();
        mapper.registerModule(new JodaModule());
        converter.setObjectMapper(mapper);
        template.getMessageConverters().add(converter);

        return template;
    }

    public static RestTemplate defaultJacksonTemplate() {
        return defaultJacksonTemplate(5000, 5000);
    }

    public static String internalNameToHumanName(String name) {
        if(name.equalsIgnoreCase("ft-substitution")) {
            return "Schüler-Vertretungsplan";
        } else if(name.equalsIgnoreCase("ft-news")) {
            return "Neuigkeiten";
        }

        return "???";
    }
}
