package app.planomy.cmassive.de.planomy.ui.news;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.widget.TextView;

import app.planomy.cmassive.de.planomy.R;
import app.planomy.cmassive.de.planomy.manager.NewsManager;
import app.planomy.cmassive.de.planomy.model.news.News;

public class NewsDetailActivity extends AppCompatActivity {

    private News news;

    private TextView headerText;
    private TextView authorText;
    private TextView dateText;
    private TextView contentText;

    public NewsDetailActivity() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_news_detail);

        if(getIntent() != null) {
            news = NewsManager.getNews(getIntent().getIntExtra(NewsActivity.EXTRA_NEWS_ID, -1));

            if(news == null) {
                finish();
                return;
            }
        }

        setTitle("Neuigkeit");

        headerText = findViewById(R.id.news_detail_head);
        authorText = findViewById(R.id.news_deatil_author);
        dateText = findViewById(R.id.news_detail_date);
        contentText = findViewById(R.id.news_detail_text);

        headerText.setText(this.news.getHeadline());
        authorText.setText(this.news.getAuthor() != null ? news.getAuthor() : "Unbekannt");
        dateText.setText(this.news.getPublishedDate());
        contentText.setText(this.news.getContent());
        contentText.setMovementMethod(new ScrollingMovementMethod());
    }
}
