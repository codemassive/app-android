package app.planomy.cmassive.de.planomy.manager;

import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.ListIterator;

import app.planomy.cmassive.de.planomy.model.substitution.VpxSubstitution;
import app.planomy.cmassive.de.planomy.model.substitution.VpxSubstitutionDay;
import app.planomy.cmassive.de.planomy.model.user.VpxSubscription;

public class SubstitutionManager {

    private static HashMap<Integer, ArrayList<VpxSubstitutionDay>> SUBSTITUTIONS = new HashMap<>();

    public static HashMap<Integer, ArrayList<VpxSubstitutionDay>> getSubstitutions() {
        return SUBSTITUTIONS;
    }

    public static ArrayList<VpxSubstitutionDay> getSubstitutionDays(int schoolId) {
        ArrayList<VpxSubstitutionDay> substitutions = SUBSTITUTIONS.get(schoolId);
        return substitutions == null ? new ArrayList<VpxSubstitutionDay>() : substitutions;
    }

    public static synchronized void addSubstitutionDay(int schoolId, VpxSubstitutionDay day) {
        if (getSubstitutionDay(day.getId()) != null) deleteSubstitutionDay(day);

        ArrayList<VpxSubstitutionDay> substitutions = SUBSTITUTIONS.get(schoolId);
        if (substitutions == null) substitutions = new ArrayList<>();

        substitutions.add(day);
        SUBSTITUTIONS.put(schoolId, substitutions);
    }

    public static synchronized void deleteSubstitutionDay(VpxSubstitutionDay day) {
        for (int i : SUBSTITUTIONS.keySet()) {
            ListIterator<VpxSubstitutionDay> substitutionListIterator = SUBSTITUTIONS.get(i).listIterator();
            while (substitutionListIterator.hasNext()) {
                if (substitutionListIterator.next().getId() == day.getId()) {
                    substitutionListIterator.remove();
                    return;
                }
            }
        }
    }

    public static VpxSubstitutionDay getSubstitutionDay(int dayId) {
        for (ArrayList<VpxSubstitutionDay> substitutionArrayList : SUBSTITUTIONS.values()) {
            for (VpxSubstitutionDay day : substitutionArrayList) {
                if (day.getId() == dayId) return day;
            }
        }
        return null;
    }

    public static LinkedHashMap<String, ArrayList<VpxSubstitution>> groupByClassesAndWeekday(int schoolId, String fcls, int dayOfWeek) {
        if(SUBSTITUTIONS.get(schoolId) == null) return new LinkedHashMap<>();

        LinkedHashMap<String, ArrayList<VpxSubstitution>> map = new LinkedHashMap<>();
        for (VpxSubstitutionDay day : SUBSTITUTIONS.get(schoolId)) {
            if (dayOfWeek != -1 && day.getDate().getDayOfWeek() != dayOfWeek) {
                continue;
            }

            for (VpxSubstitution substitution : day.getSubstitutionList()) {
                for (String cls : substitution.getClasses()) {
                    if(fcls != null && !cls.equalsIgnoreCase(fcls)) break;
                    if (!map.containsKey(cls)) {
                        map.put(cls, new ArrayList<>());
                    }

                    ArrayList<VpxSubstitution> substitutions = map.get(cls);
                    substitutions.add(substitution);
                    map.put(cls, substitutions);
                }
            }
        }

        return map;
    }

    public static int countSubstitutionsForWeekday(int schoolId, int dayOfWeek, String cls) {
        if(!SUBSTITUTIONS.containsKey(schoolId)) return 0;
        int count = 0;

        for(VpxSubstitutionDay substitutionDay : SUBSTITUTIONS.get(schoolId)) {
            for(VpxSubstitution substitution : substitutionDay.getSubstitutionList()) {
                if(cls != null && !substitution.getClasses().contains(cls)) continue;

                if(substitutionDay.getDate().getDayOfWeek() == dayOfWeek) {
                    count++;
                }
            }
        }

        return count;
    }

    public static LinkedHashMap<String, ArrayList<VpxSubstitution>> groupByClassesAndWeekday(int schoolId) {
        return groupByClassesAndWeekday(schoolId, null, -1);
    }
}
