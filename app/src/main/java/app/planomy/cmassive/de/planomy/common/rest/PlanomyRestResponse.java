package app.planomy.cmassive.de.planomy.common.rest;

public class PlanomyRestResponse<RESPONSE> {

    private int statusCode;
    private RESPONSE response;

    public PlanomyRestResponse(int statusCode, RESPONSE response) {
        this.statusCode = statusCode;
        this.response = response;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public RESPONSE getResponse() {
        return response;
    }

    public void setResponse(RESPONSE response) {
        this.response = response;
    }
}
