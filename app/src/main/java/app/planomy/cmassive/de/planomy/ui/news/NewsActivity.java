package app.planomy.cmassive.de.planomy.ui.news;

import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import app.planomy.cmassive.de.planomy.R;
import app.planomy.cmassive.de.planomy.common.ClickHandler;
import app.planomy.cmassive.de.planomy.common.TaskObserver;
import app.planomy.cmassive.de.planomy.manager.SubscriptionManager;
import app.planomy.cmassive.de.planomy.model.news.News;
import app.planomy.cmassive.de.planomy.tasks.NewsLoaderTask;
import app.planomy.cmassive.de.planomy.ui.added.FeatureSelectActivity;


public class NewsActivity extends AppCompatActivity implements ClickHandler<News> {

    private int schoolId;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private NewsRecyclerAdapter adapter;

    public static final String EXTRA_NEWS_ID = "EXTRA_NEWS_ID";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);

        this.swipeRefreshLayout = findViewById(R.id.news_refresh);
        this.swipeRefreshLayout.setRefreshing(false);
        this.swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new NewsLoaderTask(SubscriptionManager.getSubscription(schoolId, "ft-news"), new TaskObserver<News[]>() {
                    @Override
                    public void onSuccess(News[] news) {
                        swipeRefreshLayout.setRefreshing(false);
                        recyclerView.getAdapter().notifyDataSetChanged();
                    }

                    @Override
                    public void onError(Exception ex) {

                    }
                }).execute();
            }
        });

        this.recyclerView = findViewById(R.id.news_list);
        this.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        this.recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        handleIntent(getIntent());

        setTitle("Neuigkeiten");
        Log.w("Warn", "BUNDLE NEW ACTIVITY!");
    }

    @Override
    public void onClick(News object) {
        Intent intent = new Intent(this, NewsDetailActivity.class);
        intent.putExtra(EXTRA_NEWS_ID, object.getNewsId());
        startActivity(intent);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);

        handleIntent(getIntent());
    }

    private void handleIntent(Intent intent) {
        if(intent == null) Log.w("Warn", "BUNDLE INTENT NULL!");
        if(!intent.hasExtra(FeatureSelectActivity.EXTRA_SCHOOL_ID)) Log.w("Warn", "BUNDLE SCHOOL ID NULL");
        if(intent != null && intent.hasExtra(FeatureSelectActivity.EXTRA_SCHOOL_ID)) {
            for(String s : intent.getExtras().keySet()) {
                Log.w("Warn", "BUNDLE " + s + ":" + intent.getExtras().get(s).toString());
            }

            schoolId = getIntent().getIntExtra(FeatureSelectActivity.EXTRA_SCHOOL_ID, -1);
            if(schoolId == -1) {
                finish();
                return;
            }

            this.adapter = new NewsRecyclerAdapter(this.schoolId, this);
            recyclerView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }
    }
}
