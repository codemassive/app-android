package app.planomy.cmassive.de.planomy.tasks;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import app.planomy.cmassive.de.planomy.common.TaskObserver;

public class ImageViewUrlTask extends AsyncTask<Void, Void, Bitmap> {

    private URL url;
    private ImageView view;
    private TaskObserver<Bitmap> taskObserver;

    public ImageViewUrlTask(URL url, ImageView view, TaskObserver<Bitmap> taskObserver) {
        this.url = url;
        this.view = view;
        this.taskObserver = taskObserver;
    }

    @Override
    protected Bitmap doInBackground(Void... voids) {
        try {
            URLConnection connection = this.url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            return BitmapFactory.decodeStream(input);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        if(bitmap != null) {
            this.view.setImageBitmap(bitmap);
        }

        if(this.taskObserver != null) {
            this.taskObserver.onSuccess(bitmap);
        }
    }
}
