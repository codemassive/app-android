package app.planomy.cmassive.de.planomy.ui.recycler;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.RecyclerView;
import android.util.Pair;
import android.util.TypedValue;
import android.view.View;

import app.planomy.cmassive.de.planomy.R;

public class SectionDivider extends DividerItemDecoration {

    private Paint paint;
    private int dynamicHeight;

    /**
     * Creates a divider {@link RecyclerView.ItemDecoration} that can be used with a
     * {@link LinearLayoutManager}.
     *
     * @param context     Current context, it will be used to access resources.
     * @param orientation Divider orientation. Should be {@link #HORIZONTAL} or {@link #VERTICAL}.
     */
    public SectionDivider(Context context, int orientation) {
        super(context, orientation);

        paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(ContextCompat.getColor(context, android.R.color.darker_gray));
        dynamicHeight = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dynamicHeight, context.getResources().getDisplayMetrics());
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        if(parent.getAdapter().getItemViewType(parent.getChildAdapterPosition(view)) == SectionAdapter.TYPE_ITEM) {
            super.getItemOffsets(outRect, view, parent, state);
        } else {
            outRect.setEmpty();
        }
    }

    @Override
    public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
        SectionAdapter adapter = null;
        if(parent.getAdapter() instanceof SectionAdapter) {
            adapter = (SectionAdapter) parent.getAdapter();
        }

        for (int i = 0; i < parent.getChildCount(); i++) {
            View view = parent.getChildAt(i);
            int position = parent.getChildAdapterPosition(view);
            int viewType = parent.getAdapter().getItemViewType(position);

            if (viewType == SectionAdapter.TYPE_ITEM) {
                if(adapter != null) {
                    Pair<Integer, Integer> pair = adapter.getSectionPosComplex(position);
                    int totalCount = adapter.getProvider().getSectionItemCount(pair.first);

                    if(pair.second == totalCount-1) {
                        continue;
                    }
                }

                c.drawRect(view.getLeft(), view.getBottom(), view.getRight(), view.getBottom() + 2, paint);
            }
        }
    }
}
