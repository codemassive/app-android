package app.planomy.cmassive.de.planomy.ui.settings;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.Switch;

import app.planomy.cmassive.de.planomy.R;
import app.planomy.cmassive.de.planomy.model.settings.VpxNewsSettings;
import app.planomy.cmassive.de.planomy.model.user.VpxSubscription;

public class NewsSettingsActivity extends SettingsActivityBase {

    private Switch notifySwitch;

    public NewsSettingsActivity() {

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_news);

        notifySwitch = findViewById(R.id.settings_notify);
    }

    @Override
    public void saveSettings(VpxSubscription subscription) {
        VpxNewsSettings newsSettings = new VpxNewsSettings();
        if(subscription.getLocalSettings() != null) {
            newsSettings = (VpxNewsSettings) subscription.getLocalSettings();
        }

        newsSettings.setNotifications(notifySwitch.isChecked());
        subscription.setLocalSettings(newsSettings);
    }

    @Override
    public void loadSettings(VpxSubscription subscription) {
        if(subscription.getLocalSettings() != null) {
            notifySwitch.setChecked(((VpxNewsSettings) subscription.getLocalSettings()).isNotifications());
        }
    }
}
