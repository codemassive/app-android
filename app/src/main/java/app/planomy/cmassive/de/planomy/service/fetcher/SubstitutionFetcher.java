package app.planomy.cmassive.de.planomy.service.fetcher;

import app.planomy.cmassive.de.planomy.manager.SubstitutionManager;
import app.planomy.cmassive.de.planomy.model.substitution.VpxSubstitution;
import app.planomy.cmassive.de.planomy.model.substitution.VpxSubstitutionDay;
import app.planomy.cmassive.de.planomy.model.user.VpxSubscription;
import app.planomy.cmassive.de.planomy.tasks.SubstitutionLoaderTask;

public class SubstitutionFetcher implements FetcherBase {

    @Override
    public String getName() {
        return "Vertretungsplan";
    }

    @Override
    public boolean fetchData(VpxSubscription subscription) {
        boolean n = false;

        try {
            VpxSubstitutionDay days[] = SubstitutionLoaderTask.fetchSubstitutions(subscription.getSchoolId());
            for(VpxSubstitutionDay day : days) {
                if(!n) {
                    if(SubstitutionManager.getSubstitutionDay(day.getId()) == null) {
                        n = true;
                    }
                }

                SubstitutionManager.addSubstitutionDay(subscription.getSchoolId(), day);
            }
        } catch(Exception ex) {

        }

        return n;
    }
}
