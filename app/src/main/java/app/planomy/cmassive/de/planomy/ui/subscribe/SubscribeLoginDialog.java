package app.planomy.cmassive.de.planomy.ui.subscribe;

import android.app.ActionBar;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import app.planomy.cmassive.de.planomy.R;

public class SubscribeLoginDialog extends Dialog {

    private LoginActionListener listener = null;

    private TextView usernameView;
    private TextView passwordView;
    private CheckBox protectionBox;

    public SubscribeLoginDialog(@NonNull Context context, LoginActionListener loginActionListener) {
        super(context);

        this.listener = loginActionListener;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_login);

        usernameView = findViewById(R.id.dialog_login_username);
        passwordView = findViewById(R.id.dialog_login_password);
        protectionBox = findViewById(R.id.dialog_login_protection);

        Button abortBtn = findViewById(R.id.dialog_button_abort);
        abortBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onAbort();
            }
        });

        Button okBtn = findViewById(R.id.dialog_button_ok);
        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onAccept();
            }
        });

        getWindow().setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        setCancelable(false);
    }

    public void clear() {
        if(usernameView != null && passwordView != null) {
            usernameView.setText("");
            passwordView.setText("");
        }
    }

    public LoginActionListener getListener() {
        return listener;
    }

    public void setListener(LoginActionListener listener) {
        this.listener = listener;
    }

    public TextView getUsernameView() {
        return usernameView;
    }

    public void setUsernameView(TextView usernameView) {
        this.usernameView = usernameView;
    }

    public TextView getPasswordView() {
        return passwordView;
    }

    public void setPasswordView(TextView passwordView) {
        this.passwordView = passwordView;
    }

    public boolean isProtectionChecked() {
        return this.protectionBox.isChecked();
    }

    public CheckBox getProtectionBox() {
        return protectionBox;
    }

    public interface LoginActionListener {

        void onAccept();
        void onAbort();

    }
}
