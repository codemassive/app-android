package app.planomy.cmassive.de.planomy.manager.local;

import java.util.ArrayList;

public class LocalSettings {

    private String userId;
    private ArrayList<LocalSubscriptionSettings> localSettings = new ArrayList<>();

    public LocalSettings() {

    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public ArrayList<LocalSubscriptionSettings> getLocalSettings() {
        return localSettings;
    }

    public void setLocalSettings(ArrayList<LocalSubscriptionSettings> localSettings) {
        this.localSettings = localSettings;
    }
}
