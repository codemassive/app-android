package app.planomy.cmassive.de.planomy.model.settings;

import java.util.HashMap;

public class VpxDefaultSettings extends VpxSettings {

    private boolean notifications;

    public boolean isNotifications() {
        return notifications;
    }

    public void setNotifications(boolean notifications) {
        this.notifications = notifications;
    }
}
