package app.planomy.cmassive.de.planomy.tasks;

import android.os.AsyncTask;
import android.util.Log;

import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import app.planomy.cmassive.de.planomy.common.rest.PlanomyRestCall;
import app.planomy.cmassive.de.planomy.common.rest.PlanomyUrls;
import app.planomy.cmassive.de.planomy.common.TaskObserver;
import app.planomy.cmassive.de.planomy.common.rest.RestCommon;
import app.planomy.cmassive.de.planomy.model.school.VpxSchool;

/**
 * Created by johan on 11.04.2018.
 */

public class SchoolLoaderTask extends AsyncTask<Void, Void, VpxSchool[]> {

    private TaskObserver<VpxSchool[]> consumer;
    private Exception exception;

    public SchoolLoaderTask(TaskObserver<VpxSchool[]> consumer) {
        this.consumer = consumer;
    }

    @Override
    protected VpxSchool[] doInBackground(Void... voids) {
        try {
            this.exception = null;
            VpxSchool[] schools = loadSchools();
            return schools;
        } catch(Exception ex) {
            this.exception = ex;
            ex.printStackTrace();
        }

        return new VpxSchool[0];
    }

    @Override
    protected void onPostExecute(VpxSchool[] vpxSchools) {
        super.onPostExecute(vpxSchools);

        if(this.exception != null) {
            if(this.consumer != null) consumer.onError(exception);
            return;
        }

        if(this.consumer != null) {
            if(vpxSchools != null) {
                this.consumer.onSuccess(vpxSchools);
            } else {
                this.consumer.onError(null);
            }
        }
    }

    public static VpxSchool[] loadSchools() throws Exception {
        return new PlanomyRestCall<VpxSchool[]>(PlanomyUrls.API_SCHOOLS.getUrl(), "GET", VpxSchool[].class).call().getResponse();
    }

    public static VpxSchool[] loadSchools2() {
        RestTemplate template = RestCommon.defaultJacksonTemplate();

        Log.i("Info", "Before entity!");
        ResponseEntity<VpxSchool[]> schoolResponse = template.getForEntity(PlanomyUrls.API_SCHOOLS.getUrlString(), VpxSchool[].class);
        Log.i("Info", "After entity!");
        if(schoolResponse == null || schoolResponse.getBody() == null) return new VpxSchool[0];

        return schoolResponse.getBody();
    }
}
