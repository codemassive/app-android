package app.planomy.cmassive.de.planomy.ui;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import android.widget.Toast;

import app.planomy.cmassive.de.planomy.R;
import app.planomy.cmassive.de.planomy.common.ClickHandler;
import app.planomy.cmassive.de.planomy.common.TaskObserver;
import app.planomy.cmassive.de.planomy.manager.SchoolManager;
import app.planomy.cmassive.de.planomy.model.school.VpxSchool;
import app.planomy.cmassive.de.planomy.tasks.SchoolLoaderTask;
import app.planomy.cmassive.de.planomy.ui.subscribe.SubscriptionActivity;

public class SchoolBrowseFragment extends Fragment implements ClickHandler<VpxSchool> {

    private SchoolListFragment schoolListFragment;

    public static final String EXTRA_SCHOOL_ID = "EXTRA_SCHOOL_ID";

    public SchoolBrowseFragment() {
        // Required empty public constructor
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_school_browse, container, false);

        this.schoolListFragment = (SchoolListFragment) getChildFragmentManager().findFragmentById(R.id.browse_school_list);
        this.schoolListFragment.setListener(this);
        final SearchView searchView = view.findViewById(R.id.browse_school_search);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                schoolListFragment.getRecyclerViewAdapter().setQuery(s, true);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });

        final SwipeRefreshLayout swipeRefreshLayout = view.findViewById(R.id.school_refresh);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new SchoolLoaderTask(new TaskObserver<VpxSchool[]>() {
                    @Override
                    public void onSuccess(VpxSchool[] vpxSchools) {
                        synchronized (SchoolManager.getSchools()) {
                            SchoolManager.getSchools().clear();
                            for(VpxSchool school : vpxSchools) {
                                SchoolManager.addSchool(school);
                            }
                        }

                        schoolListFragment.getRecyclerViewAdapter().notifyDataSetChanged();
                        swipeRefreshLayout.setRefreshing(false);
                        Toast.makeText(getContext(), "Schulen wurden aktualisiert.", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onError(Exception ex) {
                        Toast.makeText(getContext(), "Fehler beim Laden der Schulen!", Toast.LENGTH_LONG).show();
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }).execute();
            }
        });

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public SchoolListFragment getSchoolListFragment() {
        return schoolListFragment;
    }

    public void notifySchoolChange() {
        this.schoolListFragment.notifySchoolsChanged();
    }

    @Override
    public void onClick(VpxSchool object) {
        Intent intent = new Intent(getContext(), SubscriptionActivity.class);
        intent.putExtra(EXTRA_SCHOOL_ID, object.getSchoolId());
        startActivity(intent);
        Log.i("Info", "REP!");
    }
}
