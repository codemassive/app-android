package app.planomy.cmassive.de.planomy.ui.subscribe;

import app.planomy.cmassive.de.planomy.R;
import app.planomy.cmassive.de.planomy.common.ClickHandler;
import app.planomy.cmassive.de.planomy.common.TaskObserver;
import app.planomy.cmassive.de.planomy.manager.SchoolManager;
import app.planomy.cmassive.de.planomy.manager.SubscriptionManager;
import app.planomy.cmassive.de.planomy.manager.UserDataManager;
import app.planomy.cmassive.de.planomy.model.school.VpxSchool;
import app.planomy.cmassive.de.planomy.model.substitution.VpxSubstitution;
import app.planomy.cmassive.de.planomy.model.user.VpxSubscription;
import app.planomy.cmassive.de.planomy.tasks.NewsLoaderTask;
import app.planomy.cmassive.de.planomy.tasks.SubscribeTask;
import app.planomy.cmassive.de.planomy.tasks.SubstitutionLoaderTask;
import app.planomy.cmassive.de.planomy.ui.SchoolBrowseFragment;
import app.planomy.cmassive.de.planomy.ui.SchoolInfoActivity;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import java.util.Collection;
import java.util.HashMap;
import java.util.UUID;

public class SubscriptionActivity extends AppCompatActivity implements ClickHandler<SubscriptionRecyclerAdapter.ClickObjectBundle> {

    private VpxSchool school;
    private RecyclerView recyclerView;
    private SubscribeLoginDialog dialog;

    private static HashMap<UUID, SubscribeTask> tasks = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscription);

        Intent intent = getIntent();
        school = SchoolManager.getSchool(intent.getIntExtra(SchoolInfoActivity.EXTRA_SCHOOL_ID, -1));

        setTitle("Abonnements");

        this.recyclerView = findViewById(R.id.sub_list);
        this.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        this.recyclerView.setAdapter(new SubscriptionRecyclerAdapter(school, this));
        this.recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.HORIZONTAL));
    }

    @Override
    public void onClick(SubscriptionRecyclerAdapter.ClickObjectBundle object) {
        if(object.holder.nameText.getText().toString().startsWith("???")) {
            new SubscribeUnknownDialog(this).show();
            object.holder.onOffSwitch.setChecked(false);
            return;
        }

        object.holder.onOffSwitch.setClickable(false);

        if(object.feature.isAuthRequired() && object.holder.onOffSwitch.isChecked()) {
            handleLoginFeature(object);
            return;
        }

        startSubscription(object, null);
    }

    @Override
    public void onBackPressed() {
        if(tasks.size() > 0) {
            Toast.makeText(this, "Aktionen noch nicht beendet!", Toast.LENGTH_LONG).show();
        } else {
            finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
            {
                onBackPressed();
                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    private void startSubscription(final SubscriptionRecyclerAdapter.ClickObjectBundle bundle, HashMap<String, String> loginData) {
        SubscribeTask.SubscriptionMethod method = SubscribeTask.SubscriptionMethod.SUBSCRIBE;
        final VpxSubscription subscription = SubscriptionManager.getSubscription(school.getSchoolId(), bundle.feature.getName());

        if(subscription != null) {
            method = SubscribeTask.SubscriptionMethod.UNSUBSCRIBE;
        }

        final UUID taskId = UUID.randomUUID();
        SubscribeTask task = new SubscribeTask(method, new TaskObserver<VpxSubscription>() {
            @Override
            public void onSuccess(VpxSubscription o) {
                tasks.remove(taskId);

                if(o == null) {
                    finishWith("Abonnement erfolgreich beendet!", bundle);

                    if(subscription != null) {
                        SubscriptionManager.deleteSubscription(subscription);
                    }
                } else {
                    finishWith("Abonnement erfolgreich gestartet!", bundle);
                    SubscriptionManager.addSubscription(o);

                    if(o.getFeatureName().contains("ft-news")) {
                        new NewsLoaderTask((VpxSubscription) o).execute();
                    } else if(o.getFeatureName().contains("ft-substitution")) {
                        new SubstitutionLoaderTask(o.getSchoolId(), null).execute();
                    }
                }
            }

            @Override
            public void onError(Exception ex) {
                tasks.remove(taskId);

                if(ex instanceof HttpClientErrorException) {
                    HttpClientErrorException clientError = (HttpClientErrorException) ex;
                    if(clientError.getStatusCode() == HttpStatus.UNAUTHORIZED) {
                        finishWith("Falsche Zugangsdaten!", bundle);
                    } else if(clientError.getStatusCode() == HttpStatus.BAD_REQUEST) {
                        finishWith("Interner Fehler!", bundle);
                    } else if(clientError.getStatusCode() == HttpStatus.NOT_FOUND) {
                        finishWith("Nicht gefunden!", bundle);
                    } else {
                        finishWith("Error Code " + clientError.getStatusCode().value(), bundle);
                    }
                } else {
                    finishWith("Unbekannter Fehler!", bundle);
                }
            }
        }, bundle.feature);

        if(method == SubscribeTask.SubscriptionMethod.SUBSCRIBE) {
            task.setLoginData(loginData);
            task.setSchoolId(this.school.getSchoolId());
        } else {
            task.setSubscription(subscription);
        }

        bundle.holder.progressBar.setVisibility(View.VISIBLE);

        tasks.put(taskId, task);
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void finishWith(final String msg, final SubscriptionRecyclerAdapter.ClickObjectBundle bundle) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(msg != null) {
                    Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                }

                bundle.holder.progressBar.setVisibility(View.GONE);
                recyclerView.getAdapter().notifyDataSetChanged();
            }
        });
    }

    private void handleLoginFeature(final SubscriptionRecyclerAdapter.ClickObjectBundle bundle) {
        dialog = new SubscribeLoginDialog(this, new SubscribeLoginDialog.LoginActionListener() {
            @Override
            public void onAccept() {
                if(!dialog.isProtectionChecked()) {
                    Toast.makeText(SubscriptionActivity.this, "Bitte Bestätigung ankreuzen!", Toast.LENGTH_SHORT).show();
                    return;
                }

                HashMap<String, String> loginData = new HashMap<>();
                loginData.put("password", dialog.getPasswordView().getText().toString());
                loginData.put("username", dialog.getUsernameView().getText().toString());
                startSubscription(bundle, loginData);
                dialog.dismiss();
            }

            @Override
            public void onAbort() {
                dialog.dismiss();
                recyclerView.getAdapter().notifyDataSetChanged();
            }
        });

        dialog.show();
    }
}
