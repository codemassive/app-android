package app.planomy.cmassive.de.planomy.model.settings;

import java.util.HashMap;

public class VpxSubstitutionSettings extends VpxDefaultSettings {

    private String schoolClass;

    public VpxSubstitutionSettings() {

    }

    public String getSchoolClass() {
        return schoolClass;
    }

    public void setSchoolClass(String schoolClass) {
        this.schoolClass = schoolClass;
    }
}
