package app.planomy.cmassive.de.planomy.service.fetcher;

import android.util.Log;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import app.planomy.cmassive.de.planomy.common.rest.PlanomyUrl;
import app.planomy.cmassive.de.planomy.common.rest.PlanomyUrls;
import app.planomy.cmassive.de.planomy.common.rest.RestCommon;
import app.planomy.cmassive.de.planomy.manager.NewsManager;
import app.planomy.cmassive.de.planomy.model.news.News;
import app.planomy.cmassive.de.planomy.model.user.VpxSubscription;
import app.planomy.cmassive.de.planomy.tasks.NewsLoaderTask;

public class NewsFetcher implements FetcherBase {

    @Override
    public String getName() {
        return "Neuigkeiten";
    }

    @Override
    public boolean fetchData(final VpxSubscription subscription) {
        new NewsLoaderTask(subscription).execute();
        return false;
    }
}
