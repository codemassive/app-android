package app.planomy.cmassive.de.planomy.manager;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import app.planomy.cmassive.de.planomy.common.TaskObserver;
import app.planomy.cmassive.de.planomy.model.school.VpxSchool;
import app.planomy.cmassive.de.planomy.tasks.SchoolLoaderTask;

/**
 * Created by johan on 10.04.2018.
 */

public class SchoolManager {

    private static List<VpxSchool> SCHOOLS = new ArrayList<>();
    private static boolean schoolsLoaded = false;

    public static void addSchool(VpxSchool school) {
        if(getSchool(school.getSchoolId()) == null) {
            SCHOOLS.add(school);
        }
    }

    public static List<VpxSchool> getSchools() {
        return SCHOOLS;
    }

    public static List<VpxSchool> getSchoolsWithSubscriptions() {
        ArrayList<VpxSchool> schools = new ArrayList<>();
        for(VpxSchool school : SCHOOLS) {
            if(SubscriptionManager.getSubscriptions(school.getSchoolId()).size() > 0) {
                schools.add(school);
            }
        }
        return schools;
    }

    public static List<VpxSchool> searchSchool(String name) {
        if(name.trim().isEmpty()) return SCHOOLS;

        List<VpxSchool> found = new ArrayList<>();
        name = name.toLowerCase().trim();

        for(VpxSchool school : SCHOOLS) {
            if(school.getSchoolInfo().getName().toLowerCase().trim().contains(name) || school.getSchoolInfo().getShortName().toLowerCase().contains(name)) {
                found.add(school);
            }
        }

        return found;
    }

    public static VpxSchool getSchool(int schoolId) {
        for(VpxSchool school : SCHOOLS) {
            if(school.getSchoolId() == schoolId) {
                return school;
            }
        }

        return null;
    }

    public static void loadSchools(final TaskObserver<VpxSchool[]> consumer) {
        SCHOOLS.clear();
        new SchoolLoaderTask(new TaskObserver<VpxSchool[]>() {

            @Override
            public void onSuccess(VpxSchool[] vpxSchools) {
                if(consumer != null) consumer.onSuccess(vpxSchools);

                for(VpxSchool school : vpxSchools) {
                    SCHOOLS.add(school);
                }

                schoolsLoaded = true;
            }

            @Override
            public void onError(Exception ex) {
                if(consumer != null) consumer.onError(ex);

                schoolsLoaded = false;
            }
        }).execute();
    }

    public static boolean hasSchoolsBeenLoaded() {
        Log.i("Info", "return " + schoolsLoaded);
        return schoolsLoaded;
    }
}
