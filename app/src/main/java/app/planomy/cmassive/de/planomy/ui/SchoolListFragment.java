package app.planomy.cmassive.de.planomy.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import app.planomy.cmassive.de.planomy.R;
import app.planomy.cmassive.de.planomy.common.ClickHandler;
import app.planomy.cmassive.de.planomy.model.school.VpxSchool;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class SchoolListFragment extends Fragment {

    // TODO: Customize parameters
    private int mColumnCount = 1;

    private ClickHandler<VpxSchool> mListener;

    private RecyclerView recyclerView;
    private SchoolItemRecyclerViewAdapter recyclerViewAdapter;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public SchoolListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.school_fragment_item_list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView && recyclerView == null) {
            this.recyclerView = (RecyclerView) view;

            this.recyclerViewAdapter = new SchoolItemRecyclerViewAdapter(mListener);
            recyclerViewAdapter.setQuery("", false);
            recyclerView.setAdapter(this.recyclerViewAdapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        }

        Log.i("Info", "Return view");
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public ClickHandler<VpxSchool> getListener() {
        return mListener;
    }

    public void setListener(ClickHandler<VpxSchool> listener) {
        this.recyclerViewAdapter = new SchoolItemRecyclerViewAdapter(listener);
        this.recyclerView.setAdapter(this.recyclerViewAdapter);
        this.recyclerViewAdapter.notifyDataSetChanged();
    }

    public void notifySchoolsChanged() {
        this.recyclerViewAdapter.notifyDataSetChanged();
    }

    public SchoolItemRecyclerViewAdapter getRecyclerViewAdapter() {
        return recyclerViewAdapter;
    }
}
