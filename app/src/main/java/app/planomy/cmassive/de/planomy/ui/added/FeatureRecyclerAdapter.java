package app.planomy.cmassive.de.planomy.ui.added;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import app.planomy.cmassive.de.planomy.R;
import app.planomy.cmassive.de.planomy.common.ClickHandler;
import app.planomy.cmassive.de.planomy.common.rest.RestCommon;
import app.planomy.cmassive.de.planomy.manager.SubscriptionManager;
import app.planomy.cmassive.de.planomy.model.feature.VpxFeature;
import app.planomy.cmassive.de.planomy.model.school.VpxSchool;
import app.planomy.cmassive.de.planomy.model.user.VpxSubscription;
import app.planomy.cmassive.de.planomy.ui.settings.NewsSettingsActivity;
import app.planomy.cmassive.de.planomy.ui.settings.SubstitutionSettingsActivity;

public class FeatureRecyclerAdapter extends RecyclerView.Adapter<FeatureRecyclerAdapter.FeatureViewHolder> {

    private VpxSchool school;
    private ClickHandler<VpxFeature> clickHandler;

    public FeatureRecyclerAdapter(VpxSchool school, ClickHandler<VpxFeature> clickHandler) {
        this.school = school;
        this.clickHandler = clickHandler;
    }

    @Override
    public FeatureViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.feature_item, parent, false);
        return new FeatureViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final FeatureViewHolder holder, int position) {
        final VpxFeature feature = school.getSubscribedFeatures().get(position);

        if(feature.getName().contains("ft-substitution")) {
            holder.featureImage.setImageResource(R.drawable.ic_subject);
        } else if(feature.getName().contains("ft-news")) {
            holder.featureImage.setImageResource(R.drawable.ic_public);
        } else {
            holder.featureImage.setImageResource(R.drawable.ic_check);
        }

        holder.featureNameText.setText(RestCommon.internalNameToHumanName(feature.getName()));
        holder.featureNameText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickHandler.onClick(feature);
            }
        });
        holder.featureSettingsImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                VpxSubscription subscription = SubscriptionManager.getSubscription(school.getSchoolId(), feature.getName());
                if(subscription != null) {
                    Class<?> cls = null;
                    if(feature.getName().contains("ft-news")) {
                        cls = NewsSettingsActivity.class;
                    } else if(feature.getName().contains("ft-substitution")) {
                        cls = SubstitutionSettingsActivity.class;
                    }

                    if(cls != null) {
                        Intent intent = new Intent(holder.view.getContext(), cls);
                        intent.putExtra("EXTRA_SUB_ID", subscription.getSubscriptionId());
                        holder.view.getContext().startActivity(intent);
                    }
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return school.getSubscribedFeatures().size();
    }

    public class FeatureViewHolder extends RecyclerView.ViewHolder {

        public View view;
        public ImageView featureImage;
        public ImageView featureSettingsImage;
        public TextView featureNameText;

        public FeatureViewHolder(View itemView) {
            super(itemView);

            featureImage = itemView.findViewById(R.id.feature_image);
            featureNameText =itemView.findViewById(R.id.feature_name);
            featureSettingsImage = itemView.findViewById(R.id.feature_settings);
            view = itemView;
        }
    }

}
