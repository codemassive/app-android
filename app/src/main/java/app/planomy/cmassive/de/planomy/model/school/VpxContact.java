package app.planomy.cmassive.de.planomy.model.school;

public class VpxContact {

    private String email;
    private String phone;
    private String fax;

    public VpxContact() {

    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }
}
