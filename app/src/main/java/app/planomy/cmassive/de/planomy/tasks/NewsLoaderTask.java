package app.planomy.cmassive.de.planomy.tasks;

import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.gcm.Task;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import app.planomy.cmassive.de.planomy.common.TaskObserver;
import app.planomy.cmassive.de.planomy.common.rest.PlanomyUrls;
import app.planomy.cmassive.de.planomy.common.rest.RestCommon;
import app.planomy.cmassive.de.planomy.manager.NewsManager;
import app.planomy.cmassive.de.planomy.model.news.News;
import app.planomy.cmassive.de.planomy.model.user.VpxSubscription;

public class NewsLoaderTask extends AsyncTask<Void, Void, News[]> {

    private VpxSubscription subscription;
    private TaskObserver<News[]> taskObserver;

    public NewsLoaderTask(VpxSubscription subscription) {
        this(subscription, null);
    }

    public NewsLoaderTask(VpxSubscription subscription, TaskObserver<News[]> taskObserver) {
        this.subscription = subscription;
        this.taskObserver = taskObserver;
    }

    @Override
    protected News[] doInBackground(Void... voids) {
        try {
            return fetchNews(subscription);
        } catch(Exception ex) {
            ex.printStackTrace();

            if(this.taskObserver != null) taskObserver.onError(ex);
        }

        return new News[0];
    }

    @Override
    protected void onPostExecute(News[] news) {
        if(news == null) {
            if(taskObserver != null) taskObserver.onError(null);
            return;
        }

        //Pattern pattern = Pattern.compile("(([a-zA-z])((!)|(\\?)|(\\.)))");
        Pattern pattern = Pattern.compile("(([a-zA-z]+)((!)|(\\.)|(\\?))(\\s+)?)");

        synchronized (NewsManager.getNews()) {
            for(News n : news) {
                StringBuffer buffer = new StringBuffer();
                Matcher matcher = pattern.matcher(n.getContent());
                while(matcher.find()) {
                    String repl = matcher.group(1);
                    matcher.appendReplacement(buffer, repl.trim() + "\n");
                }
                matcher.appendTail(buffer);
                n.setContent(buffer.toString());

                if(NewsManager.getNews(n.getNewsId()) == null) {
                    NewsManager.addNews(subscription.getSchoolId(), n);
                }
            }
        }

        if(this.taskObserver != null) {
            taskObserver.onSuccess(news);
        }
    }

    public static News[] fetchNews(VpxSubscription subscription) {
        RestTemplate template = RestCommon.defaultJacksonTemplate();
        HttpEntity entity = RestCommon.defaultEntity();

        ResponseEntity<News[]> responseEntity = template.exchange(PlanomyUrls.API_NEWS.getUrlString(), HttpMethod.GET, entity, News[].class, subscription.getSchoolId());
        News[] news = responseEntity.getBody();
        return news;
    }
}
