package app.planomy.cmassive.de.planomy.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Locale;

import app.planomy.cmassive.de.planomy.R;
import app.planomy.cmassive.de.planomy.manager.SchoolManager;
import app.planomy.cmassive.de.planomy.model.school.VpxAddress;
import app.planomy.cmassive.de.planomy.model.school.VpxSchool;
import app.planomy.cmassive.de.planomy.ui.subscribe.SubscriptionActivity;

public class SchoolInfoActivity extends AppCompatActivity {

    private VpxSchool school;
    private FloatingActionButton floatingActionButton;
    private TextView schoolAddress;

    public static final String EXTRA_SCHOOL_ID = "EXTRA_SCHOOL_ID";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_school_info);

        this.floatingActionButton = findViewById(R.id.school_info_subscribe);
        this.schoolAddress = findViewById(R.id.school_info_address);

        Log.w("Warn", "INFO CREATE");

        handleIntent(getIntent());

        this.floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent1 = new Intent(getApplicationContext(), SubscriptionActivity.class);
                intent1.putExtra(EXTRA_SCHOOL_ID, school.getSchoolId());
                startActivity(intent1);
            }
        });
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        handleIntent(intent);

        Log.w("Warn", "INFO NEW INTENT");
    }


    private void handleIntent(Intent intent) {
        if(intent != null && intent.hasExtra(SchoolBrowseFragment.EXTRA_SCHOOL_ID)) {
            school = SchoolManager.getSchool(intent.getIntExtra(SchoolBrowseFragment.EXTRA_SCHOOL_ID, -1));

            Log.w("Warn", "INFO SET SCHOOL");

            if(school != null) {
                setTitle(school.getSchoolInfo().getName());

                if(school.getSchoolAddress() != null) {
                    VpxAddress a = school.getSchoolAddress();
                    String text = Locale.forLanguageTag(a.getCountry()).getCountry() + ", ";
                    text += a.getZipcode() + " " + a.getCity() + ", ";
                    text += a.getStreet() + " " + a.getHnum();

                    this.schoolAddress.setText(text);
                }

                if(school.getCachedBitmap() != null) {
                    ImageView imageView = findViewById(R.id.school_image);
                    imageView.setImageBitmap(school.getCachedBitmap());
                }
            } else {
                finish();
            }
        }
    }
}
