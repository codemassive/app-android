package app.planomy.cmassive.de.planomy.manager;

import java.util.ArrayList;
import java.util.Collection;
import java.util.ListIterator;

import app.planomy.cmassive.de.planomy.manager.local.LocalSubscriptionSettings;
import app.planomy.cmassive.de.planomy.model.settings.VpxSubstitutionSettings;
import app.planomy.cmassive.de.planomy.model.user.VpxSubscription;

public class SubscriptionManager {

    private static ArrayList<VpxSubscription> subscriptions = new ArrayList<>();

    public static synchronized VpxSubscription getSubscription(int schoolId, String ftName) {
        for(VpxSubscription subscription : subscriptions) {
            if(subscription.getFeatureName().equalsIgnoreCase(ftName) && subscription.getSchoolId() == schoolId) {
                return subscription;
            }
        }
        return null;
    }

    public static VpxSubscription getSubscription(int subId) {
        for (VpxSubscription subscription : subscriptions) {
            if(subscription.getSubscriptionId() == subId) {
                return subscription;
            }
        }
        return null;
    }

    public static ArrayList<VpxSubscription> getSubscriptions(int schoolId) {
        ArrayList<VpxSubscription> subscriptions = new ArrayList<>();
        for(VpxSubscription subscription : SubscriptionManager.subscriptions) {
            if(subscription.getSchoolId() == schoolId) {
                subscriptions.add(subscription);
            }
        }
        return subscriptions;
    }

    public static synchronized void addSubscription(VpxSubscription subscription) {
        LocalSubscriptionSettings settings = UserDataManager.getLocalSettings(subscription.getSubscriptionId());
        if(settings != null) subscription.setLocalSettings(settings.getSettings());
        subscriptions.add(subscription);
    }

    public static synchronized void addSubscriptions(Collection<VpxSubscription> subscriptions) {
        for(VpxSubscription subscription : subscriptions) {
            addSubscription(subscription);
        }
    }

    public static synchronized boolean deleteSubscription(VpxSubscription subscription) {
        ListIterator<VpxSubscription> listIterator = subscriptions.listIterator();
        while(listIterator.hasNext()) {
            if(listIterator.next().getSubscriptionId() == subscription.getSubscriptionId()) {
                listIterator.remove();
                return true;
            }
        }
        return false;
    }


    public static ArrayList<VpxSubscription> getSubscriptions() {
        return subscriptions;
    }
}
