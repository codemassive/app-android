package app.planomy.cmassive.de.planomy.ui.developer;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.LinkedHashMap;
import java.util.LinkedList;

import app.planomy.cmassive.de.planomy.R;
import app.planomy.cmassive.de.planomy.manager.UserDataManager;
import app.planomy.cmassive.de.planomy.ui.recycler.SectionAdapter;
import app.planomy.cmassive.de.planomy.ui.recycler.SectionDivider;
import app.planomy.cmassive.de.planomy.ui.recycler.SimpleDataProvider;
import app.planomy.cmassive.de.planomy.ui.recycler.SimpleItem;

public class DeveloperInfoActivity extends AppCompatActivity {

    private RecyclerView recyclerView;

    private static LinkedHashMap<String, LinkedList<SimpleItem>> DATA = new LinkedHashMap<>();

    static {
        LinkedList<SimpleItem> common = new LinkedList<>();
        common.add(new SimpleItem("Interne UserID", UserDataManager.getUserId(), null));
        common.add(new SimpleItem("Momentaner API-Endpoint", "https://api.planomy.de", null));
        common.add(new SimpleItem("Momentaner CONTENT-Endpoint", "https://content.planomy.de", null));
        common.add(new SimpleItem("Erforderliche Header für Request", "VPX_USER_IDENT", null));

        DATA.put("Allgemein", common);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_developer_info);

        setTitle("Entwicklerinformationen");

        SimpleDataProvider provider = new SimpleDataProvider(null);
        provider.setData(DATA);

        recyclerView = findViewById(R.id.developer_info_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new SectionAdapter(provider));
        recyclerView.addItemDecoration(new SectionDivider(this, DividerItemDecoration.VERTICAL));
    }


}
