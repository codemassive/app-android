package app.planomy.cmassive.de.planomy.model.substitution;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import org.joda.time.LocalDate;

import java.util.HashSet;
import java.util.Set;

public class VpxSubstitutionDay {

    @JsonProperty("dayId")
    private int id;

    @JsonIgnore
    private int accountId;

    @JsonProperty("substitutions")
    private Set<VpxSubstitution> substitutionList = new HashSet<>();

    private LocalDate date;
    private String messageOfTheDay;

    public VpxSubstitutionDay(LocalDate date) {
        this.date = date;
    }

    public VpxSubstitutionDay() {

    }

    public void addSubstitution(VpxSubstitution substitution) {
        substitutionList.add(substitution);
    }

    public void addSubstitutions(Set<VpxSubstitution> substitutions) {
        substitutionList.addAll(substitutions);
    }

    public Set<VpxSubstitution> getSubstitutionList() {
        return substitutionList;
    }

    public void setSubstitutionList(Set<VpxSubstitution> substitutionList) {
        this.substitutionList = substitutionList;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getMessageOfTheDay() {
        return messageOfTheDay;
    }

    public void setMessageOfTheDay(String messageOfTheDay) {
        this.messageOfTheDay = messageOfTheDay;
    }

    public VpxSubstitution getSubstitution(VpxSubstitution time) {
        for(VpxSubstitution sub : getSubstitutionList()) {
            if(sub.getDateTime().equals(time)) {
                return sub;
            }
        }

        return null;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }
}
