package app.planomy.cmassive.de.planomy.manager;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import app.planomy.cmassive.de.planomy.MainActivity;
import app.planomy.cmassive.de.planomy.manager.local.LocalSettings;
import app.planomy.cmassive.de.planomy.manager.local.LocalSubscriptionSettings;
import app.planomy.cmassive.de.planomy.model.user.VpxSubscription;

/**
 * Created by johan on 20.04.2018.
 */

public class UserDataManager {

    private static LocalSettings _settings;
    private static ObjectMapper _mapper = new ObjectMapper();

    public static LocalSettings getSettings() {
        return _settings;
    }

    public static String getUserId() {
        return _settings.getUserId();
    }

    public static void setUserId(String userId) {
        _settings.setUserId(userId);
    }

    public static ArrayList<LocalSubscriptionSettings> getLocalSettings() {
        return _settings.getLocalSettings();
    }

    public static LocalSubscriptionSettings getLocalSettings(int subscriptionId) {
        for(LocalSubscriptionSettings settings : _settings.getLocalSettings()) {
            if(settings.getId() == subscriptionId) {
                return settings;
            }
        }
        return null;
    }

    public static boolean hasDataBeenLoaded() {
        return _settings != null;
    }

    public static boolean loadData() {
        String content = readFileOrCreate("userData.json");

        if(content.trim().isEmpty()) {
            _settings = new LocalSettings();
            return true;
        }

        try {
            _settings = _mapper.readValue(content, LocalSettings.class);
        } catch(Exception ex) {
            ex.printStackTrace();
            return false;
        }

        return true;
    }

    public static boolean saveData() {
        File file = getFileAndCreate("userData.json");
        storeSubscriptionSettings();
        try {
            _mapper.writeValue(file, _settings);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    private static void storeSubscriptionSettings() {
        ArrayList<LocalSubscriptionSettings> settings = new ArrayList<>();
        for(VpxSubscription subscription : SubscriptionManager.getSubscriptions()) {
            if(subscription.getLocalSettings() != null) {
                settings.add(new LocalSubscriptionSettings(subscription.getFeatureName(), subscription.getSubscriptionId(), subscription.getLocalSettings()));
            }
        }

        _settings.setLocalSettings(settings);
    }

    private static String readFileOrCreate(String fileName) {
        String content = "";
        File file = getFileAndCreate(fileName);

        try {
            BufferedReader reader = new BufferedReader(new FileReader(file));
            String line = "";
            while((line = reader.readLine()) != null) {
                content += line;
            }

            return content == null ? "" : content;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return content == null ? "" : content;
    }

    private static File getFileAndCreate(String fileName) {
        File file = new File(MainActivity.getStaticContext().getFilesDir(), fileName);
        if(!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return file;
    }

}
