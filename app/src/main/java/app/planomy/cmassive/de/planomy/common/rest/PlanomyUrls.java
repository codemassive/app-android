package app.planomy.cmassive.de.planomy.common.rest;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by johan on 11.04.2018.
 */

public class PlanomyUrls {

    private static final String HOST = "https://api.planomy.de";

    public static PlanomyUrl API_SCHOOLS;
    public static PlanomyUrl API_REGISTER;

    public static PlanomyUrl API_USER;
    public static PlanomyUrl API_USER_SUBSCRIPTIONS;
    public static PlanomyUrl API_USER_SUBSCRIPTION;

    public static PlanomyUrl API_NEWS;
    public static PlanomyUrl API_SUBSTITUTIONS;

    public static PlanomyUrl API_CONTENT;

    static {
        API_SCHOOLS = buildPath("schools");
        API_REGISTER = buildPath("register");
        API_USER = buildPath("user");
        API_USER_SUBSCRIPTIONS = buildPath("user/subscriptions");
        API_USER_SUBSCRIPTION = buildPath("user/subscriptions/{id}");
        API_NEWS = buildPath("schools/{schoolId}/news");
        API_SUBSTITUTIONS = buildPath("schools/{schoolId}/substitutions");

        try {
            API_CONTENT = new PlanomyUrl(new URL("https://content.planomy.de"));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    private static PlanomyUrl buildPath(String apiPath) {
        URL url = null;
        try {
            url = new URL(HOST + "/" + apiPath);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return new PlanomyUrl(url);
    }

}
