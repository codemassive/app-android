package app.planomy.cmassive.de.planomy.service.fetcher;

import app.planomy.cmassive.de.planomy.model.user.VpxSubscription;

public interface FetcherBase {

    String getName();
    boolean fetchData(VpxSubscription subscription);

}
