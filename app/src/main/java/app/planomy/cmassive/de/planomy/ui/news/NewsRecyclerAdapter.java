package app.planomy.cmassive.de.planomy.ui.news;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import app.planomy.cmassive.de.planomy.R;
import app.planomy.cmassive.de.planomy.common.ClickHandler;
import app.planomy.cmassive.de.planomy.common.TaskObserver;
import app.planomy.cmassive.de.planomy.manager.NewsManager;
import app.planomy.cmassive.de.planomy.model.news.News;
import app.planomy.cmassive.de.planomy.tasks.ImageViewUrlTask;

public class NewsRecyclerAdapter extends RecyclerView.Adapter<NewsRecyclerAdapter.NewsViewHolder> {

    private int schoolId;
    private ClickHandler<News> clickHandler;

    public NewsRecyclerAdapter(int schoolId, ClickHandler<News> clickHandler) {
        this.schoolId = schoolId;
        this.clickHandler = clickHandler;
    }

    @Override
    public NewsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.news_item, parent, false);
        return new NewsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(NewsViewHolder holder, int position) {
        List<News> news = NewsManager.getNewsForSchool(this.schoolId);
        final News used = news.get(position);

        holder.headerText.setText(used.getHeadline());
        if(used.getCachedImage() != null) {
            holder.imageView.setImageBitmap(used.getCachedImage());
        } else if(used.getMainImageUrl() != null) {
            try {
                new ImageViewUrlTask(new URL(used.getMainImageUrl()), holder.imageView, new TaskObserver<Bitmap>() {
                    @Override
                    public void onSuccess(Bitmap bitmap) {
                        used.setCachedImage(bitmap);
                    }

                    @Override
                    public void onError(Exception ex) {

                    }
                }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }

        holder.authorText.setText(used.getAuthor() != null ? used.getAuthor() : "Unbekannt");
        holder.dateText.setText(used.getPublishedDate() != null ? used.getPublishedDate() : "Unbekannt");

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(clickHandler != null) {
                    clickHandler.onClick(used);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return NewsManager.getNewsForSchool(schoolId).size();
    }

    public class NewsViewHolder extends RecyclerView.ViewHolder {

        public View view;
        public ImageView imageView;
        public TextView headerText;
        public TextView dateText;
        public TextView authorText;

        public NewsViewHolder(View itemView) {
            super(itemView);
            this.view = itemView;

            headerText = view.findViewById(R.id.news_item_head);
            imageView = view.findViewById(R.id.news_item_image);
            dateText = view.findViewById(R.id.news_item_date);
            authorText = view.findViewById(R.id.news_item_author);
        }
    }

}
