package app.planomy.cmassive.de.planomy.model.substitution;

import android.support.annotation.NonNull;

import org.joda.time.LocalDateTime;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class VpxSubstitution implements Comparable<VpxSubstitution> {

    private Set<String> newTeacher, oldTeacher;
    private Set<String> classes;
    private String newSubject, oldSubject;
    private String oldRoom, newRoom;
    private String description;

    private String lessonString;
    private LocalDateTime dateTime;

    private int id;

    public VpxSubstitution() {
        newTeacher = new HashSet<>();
        oldTeacher = new HashSet<>();
        classes = new HashSet<>();
    }

    public String getLessonString() {
        return lessonString;
    }

    public void setLessonString(String lessonString) {
        this.lessonString = lessonString;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public Set<String> getNewTeacher() {
        return newTeacher;
    }

    public void setNewTeacher(Set<String> newTeacher) {
        this.newTeacher = newTeacher;
    }

    public Set<String> getOldTeacher() {
        return oldTeacher;
    }

    public void setOldTeacher(Set<String> oldTeacher) {
        this.oldTeacher = oldTeacher;
    }

    public Set<String> getClasses() {
        return classes;
    }

    public void setClasses(Set<String> classes) {
        this.classes = classes;
    }

    public String getNewSubject() {
        return newSubject;
    }

    public void setNewSubject(String newSubject) {
        this.newSubject = newSubject;
    }

    public String getOldSubject() {
        return oldSubject;
    }

    public void setOldSubject(String oldSubject) {
        this.oldSubject = oldSubject;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOldRoom() {
        return oldRoom;
    }

    public void setOldRoom(String oldRoom) {
        this.oldRoom = oldRoom;
    }

    public String getNewRoom() {
        return newRoom;
    }

    public void setNewRoom(String newRoom) {
        this.newRoom = newRoom;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null) return false;
        if(!(obj instanceof VpxSubstitution)) return false;

        VpxSubstitution other = (VpxSubstitution) obj;

        if(
                Objects.equals(other.getClasses(), getClasses()) &&
                        Objects.equals(other.getDateTime(), getDateTime()) &&
                        Objects.equals(other.getDescription(), getDescription()) &&
                        Objects.equals(other.getLessonString(), getLessonString()) &&
                        Objects.equals(other.getNewSubject(), getNewSubject()) &&
                        Objects.equals(other.getOldSubject(), getOldSubject()) &&
                        Objects.equals(other.getNewRoom(), getNewRoom()) &&
                        Objects.equals(other.getOldRoom(), getOldRoom())
                ) {
            return true;
        }

        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(newTeacher, oldTeacher, classes, newSubject, oldSubject, oldRoom, newRoom, description, dateTime);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public int compareTo(@NonNull VpxSubstitution substitution) {
        return this.dateTime.compareTo(substitution.getDateTime());
    }
}
