package app.planomy.cmassive.de.planomy.service;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import app.planomy.cmassive.de.planomy.MainActivity;
import app.planomy.cmassive.de.planomy.R;
import app.planomy.cmassive.de.planomy.manager.SchoolManager;
import app.planomy.cmassive.de.planomy.manager.SubscriptionManager;
import app.planomy.cmassive.de.planomy.manager.UserDataManager;
import app.planomy.cmassive.de.planomy.model.user.VpxSubscription;
import app.planomy.cmassive.de.planomy.service.fetcher.FetcherBase;
import app.planomy.cmassive.de.planomy.service.fetcher.NewsFetcher;
import app.planomy.cmassive.de.planomy.service.fetcher.SubstitutionFetcher;
import io.hypertrack.smart_scheduler.Job;
import io.hypertrack.smart_scheduler.SmartScheduler;

public class RestJob implements SmartScheduler.JobScheduledCallback {

    public static final long JOB_INTERVAL = 1000*60*5; //5 minutes
    public static final int JOB_ID = Job.generateJobID();

    private static boolean isInitialTask = true;

    private static HashMap<String, FetcherBase> fetcherBases;

    static {
        fetcherBases = new HashMap<>();
        fetcherBases.put("ft-substitution", new SubstitutionFetcher());
        fetcherBases.put("ft-news", new NewsFetcher());
    }

    @Override
    public void onJobScheduled(Context context, Job job) {
         new Thread(new Runnable() {
             @Override
             public void run() {
                 synchronized (SubscriptionManager.getSubscriptions()) {
                     List<VpxSubscription> subscriptions = SubscriptionManager.getSubscriptions();
                     String notification = "";

                     for(int i = 0; i < subscriptions.size(); i++) {
                         VpxSubscription subscription = subscriptions.get(i);
                         FetcherBase base = fetcherBases.get(subscription.getFeatureName());

                         if(base.fetchData(subscription) && !isInitialTask) {
                             notification += base.getName() + " (" + SchoolManager.getSchool(subscription.getSchoolId()).getSchoolInfo().getShortName() + ")";

                             if(i + 1 != subscriptions.size()) {
                                 notification += ", ";
                             }
                         }
                     }

                     if(!notification.isEmpty()) {
                         Intent intent = new Intent(context, MainActivity.class);
                         intent.putExtra("state", "MY_SCHOOLS");
                         intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                         PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);

                         NotificationCompat.Builder builder = new NotificationCompat.Builder(context, "PLANOMY_CHANNEL")
                                 .setSmallIcon(R.drawable.ic_subject)
                                 .setContentText("Es gibt neues bei: " + notification)
                                 .setContentTitle("Planomy")
                                 .setAutoCancel(true)
                                 .setPriority(NotificationCompat.PRIORITY_HIGH)
                                 .setContentIntent(pendingIntent);


                         NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                         if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                             NotificationChannel channel = new NotificationChannel("PLANOMY_CHANNEL", "Planomy Notifications", NotificationManager.IMPORTANCE_DEFAULT);
                             channel.setDescription("Planomy Notification Channel for feature changes");

                             notificationManager.createNotificationChannel(channel);
                         }
                         notificationManager.notify(0, builder.build());
                     }
                 }

                 isInitialTask = false;
             }
         }).start();
    }

    private boolean handleSubstitutionSubscription(VpxSubscription subscription) {
        return true;
    }

    private boolean handleNewsSubscription(VpxSubscription subscription) {
        return true;
    }
}
