package app.planomy.cmassive.de.planomy.ui.recycler;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.Pair;
import android.view.ViewGroup;

public class SectionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private SectionDataProvider provider;

    private int[] sectionItemRelation;

    public static final int TYPE_HEADER = 0;
    public static final int TYPE_ITEM = 1;

    public SectionAdapter(SectionDataProvider provider) {
        this.provider = provider;

        registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onChanged() {
                provider.updateRequested();
            }
        });
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == TYPE_HEADER) return provider.createSectionViewHolder(parent);
        if(viewType == TYPE_ITEM) return provider.createItemViewHolder(parent);

        throw new IllegalStateException("Illegal view type: " + viewType);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Pair<Integer, Integer> pair = getSectionPosComplex(position);
        if(pair == null) {
            Log.w("Warn", "CHECK PAIR NULL!");
            return;
        }

        Log.w("Warn", "CALL WITH " + pair.first + ":" + pair.second + " ON " + position);

        if(pair.second == -1) {
            provider.bindSectionViewHolder(holder, pair.first);
            Log.w("Warn", "CALL WITH BIND SECTION");
        } else {
            provider.bindItemViewHolder(holder, pair.first, pair.second, position);
            Log.w("Warn", "CALL WITH BIND ITEM");
        }
    }

    @Override
    public int getItemViewType(int position) {
        Pair<Integer, Integer> pair = getSectionPosComplex(position);
        if(pair == null) return -1;

        if(pair.second == -1) return TYPE_HEADER;
        return TYPE_ITEM;
    }

    public Pair<Integer, Integer> getSectionPosComplex(int abs) {
        for(int i = 0; i < sectionItemRelation.length; i++) {
            if (abs <= sectionItemRelation[i]) {
                if(i == 0) {
                    if(abs == 0) return new Pair<>(0, -1);
                    return new Pair<>(0, abs-1);
                }

                if(abs == sectionItemRelation[i-1]+1) return new Pair<>(i, -1);

                Log.w("Warn", "CALL WITH POS IN SECTION " + sectionItemRelation[i] + ";" + i);
                return new Pair<>(i, abs-(sectionItemRelation[i-1]+2));
            }
        }

        return null;
    }

    @Override
    public int getItemCount() {
        int i = provider.getSectionCount();
        int k = -1;
        this.sectionItemRelation = new int[i];

        for(int j = 0; j < provider.getSectionCount(); j++) {
            i += provider.getSectionItemCount(j);
            k += (provider.getSectionItemCount(j)+1);

            Log.w("Warn", "CALL WITH INITRELATION " + j + ";" + k);
            this.sectionItemRelation[j] = k;
        }

        Log.w("Warn", "CALL WITH");

        return i;
    }

    public SectionDataProvider getProvider() {
        return provider;
    }
}
