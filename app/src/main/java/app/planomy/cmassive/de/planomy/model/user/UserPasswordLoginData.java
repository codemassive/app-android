package app.planomy.cmassive.de.planomy.model.user;

/**
 * Created by johan on 20.04.2018.
 */

public class UserPasswordLoginData implements LoginData {

    private String username;
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
