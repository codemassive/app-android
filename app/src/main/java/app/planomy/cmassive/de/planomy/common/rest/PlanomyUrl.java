package app.planomy.cmassive.de.planomy.common.rest;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PlanomyUrl {

    private URL url;
    private Matcher argMatcher;

    private static Pattern ARG_PATTERN = Pattern.compile("\\{(.*)\\}");


    public PlanomyUrl(URL url) {
        this.url = url;
    }

    public URL getUrl() {
        return url;
    }

    public String getUrlString() {
        return this.url.toString();
    }

    public URL args(Object... args) {
        String url = this.url.toString();
        this.argMatcher = ARG_PATTERN.matcher(url);
        int pos = 0;

        while(argMatcher.find()) {
            url.replace(argMatcher.group(1), args[pos].toString());
            pos++;
        }

        try {
            return new URL(url);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        return null;
    }
}
