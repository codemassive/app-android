package app.planomy.cmassive.de.planomy.ui.recycler;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

import app.planomy.cmassive.de.planomy.R;
import app.planomy.cmassive.de.planomy.common.ClickHandler;

public class SimpleDataProvider implements SectionDataProvider {

    private HashMap<String, LinkedList<SimpleItem>> data = new HashMap<>();
    private ClickHandler<SimpleItem> clickHandler;

    public SimpleDataProvider(ClickHandler<SimpleItem> clickHandler) {
        this.clickHandler = clickHandler;
    }

    @Override
    public int getSectionCount() {
        return data.size();
    }

    @Override
    public int getSectionItemCount(int section) {
        int i = 0;
        for(String s : data.keySet()) {
            if(i == section) return data.get(s).size();
                i++;
        }
        return 0;
    }

    @Override
    public RecyclerView.ViewHolder createSectionViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_simple_header, parent, false);
        return new SimpleItemSectionHolder(view);
    }

    @Override
    public RecyclerView.ViewHolder createItemViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_simple_item, parent, false);
        return new SimpleItemViewHolder(view);
    }

    @Override
    public void bindSectionViewHolder(RecyclerView.ViewHolder holder, int section) {
        int i = 0;
        SimpleItemSectionHolder sectionHolder = (SimpleItemSectionHolder) holder;

        for(String s : data.keySet()) {
            if(i == section) {
                sectionHolder.sectionText.setText(s);
                return;
            }

            i++;
        }
    }

    @Override
    public void bindItemViewHolder(RecyclerView.ViewHolder holder, int section, int relPos, int absPos) {
        int i = 0;
        SimpleItemViewHolder itemViewHolder = (SimpleItemViewHolder) holder;

        for(String s : data.keySet()) {
            if(i == section) {
                SimpleItem item = data.get(s).get(relPos);
                itemViewHolder.contentText.setText(item.getContent());
                itemViewHolder.headerText.setText(item.getHeader());

                if(item.getContent() == null) {
                    itemViewHolder.contentText.setVisibility(View.GONE);
                }

                if(this.clickHandler != null) {
                    itemViewHolder.view.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            clickHandler.onClick(item);
                        }
                    });
                }
                return;
            }

            i++;
        }
    }

    @Override
    public void updateRequested() {

    }

    public HashMap<String, LinkedList<SimpleItem>> getData() {
        return data;
    }

    public void setData(HashMap<String, LinkedList<SimpleItem>> data) {
        this.data = data;
    }

    public void setSection(String name, LinkedList<SimpleItem> data) {
        this.data.put(name, data);
    }

    public static class SimpleItemViewHolder extends RecyclerView.ViewHolder {

        public View view;
        public TextView headerText;
        public TextView contentText;

        public SimpleItemViewHolder(View itemView) {
            super(itemView);
            this.view = itemView;

            this.headerText = view.findViewById(R.id.recycler_item_header);
            this.contentText = view.findViewById(R.id.recycler_item_content);
        }
    }

    public static class SimpleItemSectionHolder extends RecyclerView.ViewHolder {

        public View view;
        public TextView sectionText;

        public SimpleItemSectionHolder(View itemView) {
            super(itemView);
            this.view = itemView;

            this.sectionText = view.findViewById(R.id.recycler_section);
        }
    }
}
