package app.planomy.cmassive.de.planomy.tasks;

import android.os.AsyncTask;
import android.widget.Toast;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import app.planomy.cmassive.de.planomy.MainActivity;
import app.planomy.cmassive.de.planomy.common.TaskObserver;
import app.planomy.cmassive.de.planomy.common.rest.PlanomyUrl;
import app.planomy.cmassive.de.planomy.common.rest.PlanomyUrls;
import app.planomy.cmassive.de.planomy.common.rest.RestCommon;
import app.planomy.cmassive.de.planomy.manager.SubstitutionManager;
import app.planomy.cmassive.de.planomy.model.substitution.VpxSubstitution;
import app.planomy.cmassive.de.planomy.model.substitution.VpxSubstitutionDay;
import app.planomy.cmassive.de.planomy.model.user.VpxSubscription;

public class SubstitutionLoaderTask extends AsyncTask<Void, Void, VpxSubstitutionDay[]> {

    private int schoolId;
    private TaskObserver<VpxSubstitutionDay[]> taskObserver;
    private Exception exception;

    public SubstitutionLoaderTask(int schoolId, TaskObserver<VpxSubstitutionDay[]> taskObserver) {
        this.schoolId = schoolId;
        this.taskObserver = taskObserver;
    }

    @Override
    protected VpxSubstitutionDay[] doInBackground(Void... voids) {
        try {
            return fetchSubstitutions(this.schoolId);
        } catch(Exception ex) {
            ex.printStackTrace();
            this.exception = ex;
        }

        return new VpxSubstitutionDay[0];
    }

    @Override
    protected void onPostExecute(VpxSubstitutionDay[] vpxSubstitutions) {
        if(exception != null) {
            if(this.taskObserver != null) taskObserver.onError(exception);
            return;
        }

        if(taskObserver != null) {
            taskObserver.onSuccess(vpxSubstitutions);
        }

        for(VpxSubstitutionDay day : vpxSubstitutions) {
            SubstitutionManager.addSubstitutionDay(schoolId, day);
        }
    }

    public static VpxSubstitutionDay[] fetchSubstitutions(int schoolId) {
        RestTemplate template = RestCommon.defaultJacksonTemplate();
        HttpEntity entity = RestCommon.defaultEntity();

        ResponseEntity<VpxSubstitutionDay[]> responseEntity = template.exchange(PlanomyUrls.API_SUBSTITUTIONS.getUrlString(), HttpMethod.GET, entity, VpxSubstitutionDay[].class, schoolId);
        if(responseEntity != null && responseEntity.getBody() != null) {
            return responseEntity.getBody();
        }

        return new VpxSubstitutionDay[0];
    }
}
