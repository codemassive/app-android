package app.planomy.cmassive.de.planomy.model.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import app.planomy.cmassive.de.planomy.model.settings.VpxSettings;

/**
 * Created by johan on 19.04.2018.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class VpxSubscription {

    private int subscriptionId;
    private int schoolId;
    private String featureName;

    @JsonIgnore
    private LoginData loginData;

    @JsonIgnore
    private VpxSettings localSettings;

    public VpxSubscription() {
    }

    public VpxSubscription(int schoolId, String featureName, LoginData loginData) {
        this.featureName = featureName;
        this.schoolId = schoolId;
        this.loginData = loginData;
    }

    public int getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(int schoolId) {
        this.schoolId = schoolId;
    }

    public String getFeatureName() {
        return featureName;
    }

    public void setFeatureName(String featureName) {
        this.featureName = featureName;
    }

    public LoginData getLoginData() {
        return loginData;
    }

    public void setLoginData(LoginData loginData) {
        this.loginData = loginData;
    }

    public int getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(int subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public VpxSettings getLocalSettings() {
        return localSettings;
    }

    public void setLocalSettings(VpxSettings localSettings) {
        this.localSettings = localSettings;
    }
}
