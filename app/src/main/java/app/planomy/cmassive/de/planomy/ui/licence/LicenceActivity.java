package app.planomy.cmassive.de.planomy.ui.licence;

import android.content.Context;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;

import java.util.ArrayList;
import java.util.List;

import app.planomy.cmassive.de.planomy.R;
import app.planomy.cmassive.de.planomy.ui.recycler.SimpleItem;

public class LicenceActivity extends AppCompatActivity {

    private RecyclerView recyclerView;

    private static List<LicenceItem> DATA = new ArrayList<>();
    private static final Spanned LICENCE_APACHE = fromHtml("<a href=\"https://www.apache.org/licenses/LICENSE-2.0\">Apache License 2.0</a>");
    private static final Spanned LICENCE_MIT = fromHtml("<a href=\"https://opensource.org/licenses/MIT\">MIT License</a>");
    private static final Spanned LICENCE_FI = fromHtml("<a href=\"https://file000.flaticon.com/downloads/license/license.pdf\">Flaticon Basic License</a>");
    private static final Spanned LICENCE_GPL = fromHtml("<a href=\"https://www.gnu.org/copyleft/gpl.html\">GPL</a>");
    private static final Spanned LICENCE_CC_30 = fromHtml("<a href=\"https://creativecommons.org/licenses/by/3.0/\">Creative Commons 3.0 Unported</a>");

    static {
        DATA.add(new LicenceItem("CircleImageView v2.2.0", "hdodenhof", LICENCE_APACHE));
        DATA.add(new LicenceItem("spring-android v1.0.1.RELEASE", "spring-projects/spring-android", LICENCE_APACHE));
        DATA.add(new LicenceItem("smart-scheduler-android v0.0.11", "hypertrack", LICENCE_MIT));
        DATA.add(new LicenceItem("jackson-core v2.9.5", "FasterXML", LICENCE_APACHE));
        DATA.add(new LicenceItem("jackson-databind v2.9.5", "FasterXML", LICENCE_APACHE));
        DATA.add(new LicenceItem("jackson-datatype-joda v2.9.5", "FasterXML", LICENCE_APACHE));
        DATA.add(new LicenceItem("certificate-icon (3th icon of this list item)", "Icon made by https://www.flaticon.com/authors/vectors-market from www.flaticon.com", LICENCE_FI));
        DATA.add(new LicenceItem("news-icon", "Image by Nick Roach [http://www.elegantthemes.com/] via https://www.iconfinder.com", LICENCE_GPL));
        DATA.add(new LicenceItem("book-stack-icon", "Image by Boca Tutor [https://www.iconfinder.com/bocatutor] via https://www.iconfinder.com", LICENCE_CC_30));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_licence);

        setTitle("Lizenzhinweise");

        this.recyclerView = findViewById(R.id.licence_list);
        this.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        this.recyclerView.setAdapter(new LicenceAdapter(DATA));
        this.recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        this.recyclerView.getRecycledViewPool().setMaxRecycledViews(0, 0);
    }

    private static Spanned fromHtml(String text) {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) { //Android Nougat or higher
            return Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY);
        }

        return Html.fromHtml(text);
    }

}
