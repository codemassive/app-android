package app.planomy.cmassive.de.planomy.model.feature;

import android.support.annotation.NonNull;

import java.util.List;

import app.planomy.cmassive.de.planomy.common.rest.RestCommon;

/**
 * Created by johan on 08.04.2018.
 */

public class VpxFeature {

    private String name;
    private List<VpxEndpoint> endpoints;
    private boolean authRequired;

    public VpxFeature() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<VpxEndpoint> getEndpoints() {
        return endpoints;
    }

    public void setEndpoints(List<VpxEndpoint> endpoints) {
        this.endpoints = endpoints;
    }

    public boolean isAuthRequired() {
        return authRequired;
    }

    public void setAuthRequired(boolean authRequired) {
        this.authRequired = authRequired;
    }
}
