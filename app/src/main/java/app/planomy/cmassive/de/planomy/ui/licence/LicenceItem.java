package app.planomy.cmassive.de.planomy.ui.licence;

public class LicenceItem {

    private CharSequence name;
    private CharSequence copyright;
    private CharSequence licence;

    public LicenceItem(CharSequence name, CharSequence copyright, CharSequence licence) {
        this.name = name;
        this.copyright = copyright;
        this.licence = licence;
    }

    public CharSequence getName() {
        return name;
    }

    public void setName(CharSequence name) {
        this.name = name;
    }

    public CharSequence getCopyright() {
        return copyright;
    }

    public void setCopyright(CharSequence copyright) {
        this.copyright = copyright;
    }

    public CharSequence getLicence() {
        return licence;
    }

    public void setLicence(CharSequence licence) {
        this.licence = licence;
    }
}
